import React from "react";
import "./App.css";
import NavBar from "./NavBar/NavBar";
import Authentication from "../src/Authentication/LoginPage";
import RuleList from "./Rules/RuleList";
import AdsetConfig from "./Rules/AdsetConfig";
import AdsConfig from "./Rules/AdsConfig";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import "semantic-ui-css/semantic.min.css";

function App() {
  const NavRoute = ({ exact, path, component: Component }) => (
    <Route
      exact={exact}
      path={path}
      render={props => (
        <div>
          <NavBar {...props} />
          <Component {...props} />
        </div>
      )}
    />
  );

  return (
    <Router>
      <Switch>
        <Route exact path="/" render={() => <Redirect to="/login" />} />
        <Route exact path="/login" component={Authentication} />
        <NavRoute path="/rulelist" component={RuleList} />
        <NavRoute path="/adsetruleconfig" component={AdsetConfig} />
        <NavRoute path="/adsruleconfig" component={AdsConfig} />
      </Switch>
    </Router>
  );
}

export default App;
