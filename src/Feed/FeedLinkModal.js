import React, { Component } from "react";
import {
  Button,
  Header,
  Image,
  Modal,
  Grid,
  Form,
  Select,
  Checkbox,
  Radio,
  Input
} from "semantic-ui-react";

const platformDummy = [
  { key: "fb", value: "facebook", text: "Facebook" },
  { key: "snap", value: "snapchat", text: "Snapchat" }
];
const campaignDummy = [
  {
    key: "item1",
    value: "Doctor Credit Moldova December",
    text: "Doctor Credit Moldova December"
  },
  {
    key: "item2",
    value: "Forza Credit Moldova December",
    text: "Forza Credit Moldova December"
  },
  {
    key: "item3",
    value: "Super Zajam Croatia Jan",
    text: "Super Zajam Croatia Jan"
  },
  { key: "item4", value: "Bank Bazaar", text: "Bank Bazaar" }
];
const adsetDummy = [
  { key: "item1", value: "IN-18+", text: "IN-18+" },
  { key: "item1", value: "IN-20+", text: "IN-20+" }
];
class FeedLinkModal extends Component {
  inputStyle = {
    backgroundColor: "#f7f7f7",
    borderRadius: "4px",
    border: "1px solid transparent",
    borderBottom: "1px dashed #289df3",
    boxShadow: "0 0 0 0 transparent",
    color: "#16191c",
    padding: "0 16px 1px",
    height: "40px",
    transition:
      "box-shadow .15s ease-out .15s,border-color .15s,background .15s ease-out .15s,color .15s ease-out"
  };
  render() {
    return (
      <Modal
        open={this.props.showLinkFeedModal}
        onClose={this.props.handleLinkFeedModal}
      >
        <Modal.Header>Link Creatives</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <Form onSubmit={this.handleSubmit}>
              <Grid columns={1}>
                <Grid.Row>
                  <Grid.Column>
                    <Form.Field>
                      <label>Choose Platform</label>
                      <Select
                        name="adFormat"
                        style={{
                          backgroundColor: "#f7f7f7",
                          borderRadius: "4px",
                          border: "1px solid transparent",
                          borderBottom: "1px dashed #289df3",
                          boxShadow: "0 0 0 0 transparent",
                          color: "#16191c",
                          height: "40px",
                          transition:
                            "box-shadow .15s ease-out .15s,border-color .15s,background .15s ease-out .15s,color .15s ease-out"
                        }}
                        options={platformDummy}
                      />
                    </Form.Field>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
              <Grid columns={1}>
                <Grid.Row>
                  <Grid.Column>
                    <Form.Field>
                      <label>Choose Campaign</label>
                      <Select
                        name="adFormat"
                        style={{
                          backgroundColor: "#f7f7f7",
                          borderRadius: "4px",
                          border: "1px solid transparent",
                          borderBottom: "1px dashed #289df3",
                          boxShadow: "0 0 0 0 transparent",
                          color: "#16191c",
                          height: "40px",
                          transition:
                            "box-shadow .15s ease-out .15s,border-color .15s,background .15s ease-out .15s,color .15s ease-out"
                        }}
                        options={campaignDummy}
                      />
                    </Form.Field>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
              <Grid columns={1}>
                <Grid.Row>
                  <Grid.Column>
                    <Form.Field>
                      <label>Choose Adset</label>
                      <Select
                        name="adFormat"
                        style={{
                          backgroundColor: "#f7f7f7",
                          borderRadius: "4px",
                          border: "1px solid transparent",
                          borderBottom: "1px dashed #289df3",
                          boxShadow: "0 0 0 0 transparent",
                          color: "#16191c",
                          height: "40px",
                          transition:
                            "box-shadow .15s ease-out .15s,border-color .15s,background .15s ease-out .15s,color .15s ease-out"
                        }}
                        options={adsetDummy}
                      />
                    </Form.Field>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
              <Grid>
                <Grid.Row style={{ display: "flex", justifyContent: "center" }}>
                  <Form.Field>
                    <Button
                      style={{
                        backgroundColor: "#2185d0",
                        color: "white"
                      }}
                      content="Attach Creatives"
                    />
                  </Form.Field>
                </Grid.Row>
              </Grid>
            </Form>
          </Modal.Description>
        </Modal.Content>
      </Modal>
    );
  }
}
export default FeedLinkModal;
