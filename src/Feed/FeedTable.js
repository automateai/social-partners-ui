import React, { Component } from "react";
import { Icon, Table, Grid, Button, Checkbox } from "semantic-ui-react";
import FeedCreateModal from "./FeedCreateModal";
import FeedLinkModal from "./FeedLinkModal";
import FeedLinkedToModal from "./FeedLinkedToModal";
import FeedPreviewModal from "./FeedPreviewModal";

class FeedTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCreateFeedModal: false,
      showLinkFeedModal: false,
      showLinkedToModal: false,
      showPreviewModal: false
    };
  }
  componentDidMount() {}
  componentDidUpdate(prevProps) {}
  handleCreateFeedModal = () => {
    let { showCreateFeedModal } = this.state;
    this.setState({ showCreateFeedModal: !showCreateFeedModal });
  };
  handleLinkFeedModal = () => {
    let { showLinkFeedModal } = this.state;
    this.setState({ showLinkFeedModal: !showLinkFeedModal });
  };
  handleLinkedToModal = () => {
    let { showLinkedToModal } = this.state;
    this.setState({ showLinkedToModal: !showLinkedToModal });
  };
  handlePreviewModal = () => {
    let { showPreviewModal } = this.state;
    this.setState({ showPreviewModal: !showPreviewModal });
  };

  render() {
    return (
      <Grid>
        <Grid.Row>
          <Grid.Column
            style={{
              marginTop: "2%",
              marginRight: "3%",
              textAlign: "right"
            }}
          >
            <FeedCreateModal
              showCreateFeedModal={this.state.showCreateFeedModal}
              handleCreateFeedModal={this.handleCreateFeedModal}
            />
            <FeedLinkModal
              showLinkFeedModal={this.state.showLinkFeedModal}
              handleLinkFeedModal={this.handleLinkFeedModal}
            />
            <FeedLinkedToModal
              showLinkedToModal={this.state.showLinkedToModal}
              handleLinkedToModal={this.handleLinkedToModal}
            />
            <FeedPreviewModal
              showPreviewModal={this.state.showPreviewModal}
              handlePreviewModal={this.handlePreviewModal}
            />
            <Button.Group>
              <Button onClick={this.handleCreateFeedModal}>
                <Icon
                  style={{ cursor: "pointer" }}
                  disabled
                  name="add"
                  style={{ color: "green" }}
                />
                Create Feed
              </Button>
              <Button onClick={this.handleLinkFeedModal}>
                <Icon
                  style={{ cursor: "pointer" }}
                  disabled
                  name="paperclip"
                  style={{ color: "black" }}
                />
                Link Feed
              </Button>
              <Button>
                <Icon
                  style={{ cursor: "pointer" }}
                  disabled
                  name="trash"
                  style={{ color: "red" }}
                />
                Delete Feed
              </Button>
            </Button.Group>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column style={{ marginLeft: "2%", marginRight: "3%" }}>
            <Table celled striped>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell colSpan="11">
                    Feed Creation
                  </Table.HeaderCell>
                </Table.Row>
                <Table.Row>
                  <Table.HeaderCell>
                    <Checkbox />
                  </Table.HeaderCell>
                  <Table.HeaderCell>Feed Name</Table.HeaderCell>
                  <Table.HeaderCell>Linked To</Table.HeaderCell>
                  <Table.HeaderCell>Created At</Table.HeaderCell>
                  <Table.HeaderCell>Aspect Ratio</Table.HeaderCell>
                  <Table.HeaderCell>Type</Table.HeaderCell>
                  <Table.HeaderCell>Ad Format</Table.HeaderCell>
                  <Table.HeaderCell>Copy</Table.HeaderCell>
                  <Table.HeaderCell>Edit</Table.HeaderCell>
                  <Table.HeaderCell>Feed File</Table.HeaderCell>
                  <Table.HeaderCell>Preview</Table.HeaderCell>
                </Table.Row>
              </Table.Header>

              <Table.Body>
                <Table.Row>
                  <Table.Cell>
                    <Checkbox />
                  </Table.Cell>
                  <Table.Cell>Apparel</Table.Cell>
                  <Table.Cell>
                    <Icon
                      style={{ cursor: "pointer" }}
                      onClick={this.handleLinkedToModal}
                      name="align justify"
                    />
                  </Table.Cell>
                  <Table.Cell>19/11/2019</Table.Cell>
                  <Table.Cell>9:16</Table.Cell>
                  <Table.Cell>Video</Table.Cell>
                  <Table.Cell>Carousel</Table.Cell>
                  <Table.Cell></Table.Cell>
                  <Table.Cell></Table.Cell>
                  <Table.Cell>
                    <Icon
                      style={{ cursor: "pointer" }}
                      name="cloud download"
                      style={{ color: "green" }}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <Icon
                      style={{ cursor: "pointer" }}
                      onClick={this.handlePreviewModal}
                      name="align justify"
                    />
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <Checkbox />
                  </Table.Cell>
                  <Table.Cell>Apparel</Table.Cell>
                  <Table.Cell>
                    <Icon
                      style={{ cursor: "pointer" }}
                      onClick={this.handleLinkedToModal}
                      name="align justify"
                    />
                  </Table.Cell>
                  <Table.Cell>19/11/2019</Table.Cell>
                  <Table.Cell>9:16</Table.Cell>
                  <Table.Cell>Video</Table.Cell>
                  <Table.Cell>Carousel</Table.Cell>
                  <Table.Cell></Table.Cell>
                  <Table.Cell></Table.Cell>
                  <Table.Cell>
                    <Icon
                      style={{ cursor: "pointer" }}
                      onClick={this.handlePreviewModal}
                      name="cloud download"
                      style={{ color: "green" }}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <Icon
                      style={{ cursor: "pointer" }}
                      onClick={this.handlePreviewModal}
                      name="align justify"
                    />
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <Checkbox />
                  </Table.Cell>
                  <Table.Cell>Apparel</Table.Cell>
                  <Table.Cell>
                    <Icon
                      style={{ cursor: "pointer" }}
                      onClick={this.handleLinkedToModal}
                      name="align justify"
                    />
                  </Table.Cell>
                  <Table.Cell>19/11/2019</Table.Cell>
                  <Table.Cell>9:16</Table.Cell>
                  <Table.Cell>Video</Table.Cell>
                  <Table.Cell>Carousel</Table.Cell>
                  <Table.Cell></Table.Cell>
                  <Table.Cell></Table.Cell>
                  <Table.Cell>
                    <Icon
                      style={{ cursor: "pointer" }}
                      name="cloud download"
                      style={{ color: "green" }}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <Icon
                      style={{ cursor: "pointer" }}
                      onClick={this.handlePreviewModal}
                      name="align justify"
                    />
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <Checkbox />
                  </Table.Cell>
                  <Table.Cell>Apparel</Table.Cell>
                  <Table.Cell>
                    <Icon
                      style={{ cursor: "pointer" }}
                      onClick={this.handleLinkedToModal}
                      name="align justify"
                    />
                  </Table.Cell>
                  <Table.Cell>19/11/2019</Table.Cell>
                  <Table.Cell>9:16</Table.Cell>
                  <Table.Cell>Video</Table.Cell>
                  <Table.Cell>Carousel</Table.Cell>
                  <Table.Cell></Table.Cell>
                  <Table.Cell></Table.Cell>
                  <Table.Cell>
                    <Icon
                      style={{ cursor: "pointer" }}
                      name="cloud download"
                      style={{ color: "green" }}
                    />
                  </Table.Cell>
                  <Table.Cell>
                    <Icon
                      style={{ cursor: "pointer" }}
                      onClick={this.handlePreviewModal}
                      name="align justify"
                    />
                  </Table.Cell>
                </Table.Row>
              </Table.Body>
            </Table>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

export default FeedTable;
