import React, { Component } from "react";
import {
  Form,
  Dropdown,
  Grid,
  Segment,
  Button,
  Icon,
  Loader,
  Table
} from "semantic-ui-react";

const inputStyle = {
  border: "1px solid #bdbdbd"
};
const labelStyle = {
  color: "#757575"
};

const feedOption = [
  {
    key: "item1",
    text: "AdsCollectionDemo1",
    value: "AdsCollectionDemo1"
  },
  {
    key: "item2",
    text: "AdsCollectionDemo2",
    value: "AdsCollectionDemo2"
  },
  {
    key: "item3",
    text: "AdsCollectionDemo3",
    value: "AdsCollectionDemo3"
  }
];
const campaignOption = [
  {
    key: "item1",
    text: "Campaign_new",
    value: "Campaign_new"
  },
  {
    key: "item2",
    text: "Campaign_Swiggy",
    value: "Campaign_Swiggy"
  }
];
const adsetOption = [
  {
    key: "item1",
    text: "Adset_Story",
    value: "Adset_Story"
  },
  {
    key: "item2",
    text: "Adset_Story_1",
    value: "Adset_Story_1"
  }
];
const platformOption = [
  {
    key: "item1",
    text: "facebook",
    value: "facebook"
  },
  {
    key: "item2",
    text: "snapchat",
    value: "snapchat"
  }
];
const creationOption = [
  {
    key: "item1",
    text: "19/11/2019",
    value: "19/11/2019"
  },
  {
    key: "item2",
    text: "19/12/2019",
    value: "19/12/2019"
  }
];

class FeedFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {}
  componentDidUpdate(prevProps) {}
  render() {
    return (
      <div>
        <Loader active={false} />
        <Grid>
          <Grid.Row>
            <Grid.Column
              style={{ marginLeft: "2%", marginRight: "3%", marginTop: "3%" }}
            >
              <Segment
                style={{
                  borderRadius: 0
                }}
              >
                <Form>
                  <Grid style={{ marginBottom: "10px" }}>
                    <Grid.Row columns={5}>
                      <Grid.Column style={{ padding: "0 0 0 15px" }}>
                        <Form.Field>
                          <label style={labelStyle}>Feed Name</label>
                          <Dropdown
                            placeholder="Select Feed Name"
                            clearable
                            fluid
                            search
                            selection
                            options={feedOption}
                            style={inputStyle}
                            onChange={(e, { value }) => {
                              console.log(value);
                            }}
                          />
                        </Form.Field>
                      </Grid.Column>
                      <Grid.Column style={{ padding: "0 0 0 15px" }}>
                        <Form.Field>
                          <label style={labelStyle}>Linked To Campaign</label>
                          <Dropdown
                            placeholder="Linked to Campaign"
                            clearable
                            fluid
                            search
                            selection
                            options={campaignOption}
                            style={inputStyle}
                            onChange={(e, { value }) => {
                              console.log(value);
                            }}
                          />
                        </Form.Field>
                      </Grid.Column>

                      <Grid.Column style={{ padding: "0 0 0 15px" }}>
                        <Form.Field>
                          <label style={labelStyle}>Linked To Adset</label>
                          <Dropdown
                            placeholder="Linked to Adset"
                            clearable
                            fluid
                            search
                            selection
                            options={adsetOption}
                            style={inputStyle}
                            onChange={(e, { value }) => {
                              console.log(value);
                            }}
                          />
                        </Form.Field>
                      </Grid.Column>
                      <Grid.Column style={{ padding: "0 15px 0 15px" }}>
                        <Form.Field>
                          <label style={labelStyle}>Linked To Platform</label>
                          <Dropdown
                            placeholder="Linked to Platform"
                            clearable
                            fluid
                            search
                            selection
                            options={platformOption}
                            style={inputStyle}
                            onChange={(e, { value }) => {
                              console.log(value);
                            }}
                          />
                        </Form.Field>
                      </Grid.Column>
                      <Grid.Column style={{ padding: "0 15px 0 0" }}>
                        <Form.Field>
                          <label style={labelStyle}>Created At</label>
                          <Dropdown
                            placeholder="Created At"
                            clearable
                            fluid
                            search
                            selection
                            options={creationOption}
                            style={inputStyle}
                            onChange={(e, { value }) => {
                              console.log(value);
                            }}
                          />
                        </Form.Field>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </Form>
              </Segment>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

export default FeedFilter;
