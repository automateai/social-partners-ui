import React, { Component } from "react";
import {
  Button,
  Header,
  Image,
  Modal,
  Table,
  Icon,
  Menu,
  Checkbox
} from "semantic-ui-react";

const LinkedToTable = () => (
  <Table celled>
    <Table.Header>
      <Table.Row>
        <Table.HeaderCell>
          <Checkbox />
        </Table.HeaderCell>
        <Table.HeaderCell>Platform</Table.HeaderCell>
        <Table.HeaderCell>Campaign</Table.HeaderCell>
        <Table.HeaderCell>Adset</Table.HeaderCell>
      </Table.Row>
    </Table.Header>

    <Table.Body>
      <Table.Row>
        <Table.Cell>
          <Checkbox />
        </Table.Cell>
        <Table.Cell>Facebook</Table.Cell>
        <Table.Cell>UKu_Android</Table.Cell>
        <Table.Cell>IN-18+ male</Table.Cell>
      </Table.Row>
      <Table.Row>
        <Table.Cell>
          <Checkbox />
        </Table.Cell>
        <Table.Cell>Facebook</Table.Cell>
        <Table.Cell>UKu_Android</Table.Cell>
        <Table.Cell>IN-18+ male</Table.Cell>
      </Table.Row>
      <Table.Row>
        <Table.Cell>
          <Checkbox />
        </Table.Cell>
        <Table.Cell>Facebook</Table.Cell>
        <Table.Cell>UKu_Android</Table.Cell>
        <Table.Cell>IN-18+ male</Table.Cell>
      </Table.Row>
    </Table.Body>

    <Table.Footer>
      <Table.Row>
        <Table.HeaderCell colSpan="4">
          <Menu floated="right" pagination>
            <Menu.Item as="a" icon>
              <Icon name="chevron left" />
            </Menu.Item>
            <Menu.Item as="a">1</Menu.Item>
            <Menu.Item as="a">2</Menu.Item>
            <Menu.Item as="a">3</Menu.Item>
            <Menu.Item as="a">4</Menu.Item>
            <Menu.Item as="a" icon>
              <Icon name="chevron right" />
            </Menu.Item>
          </Menu>
        </Table.HeaderCell>
      </Table.Row>
    </Table.Footer>
  </Table>
);

class FeedLinkedToModal extends Component {
  render() {
    return (
      <Modal
        open={this.props.showLinkedToModal}
        onClose={this.props.handleLinkedToModal}
      >
        <Modal.Header>Link list of Creatives</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <LinkedToTable />
          </Modal.Description>
        </Modal.Content>
      </Modal>
    );
  }
}
export default FeedLinkedToModal;
