import React, { Component } from "react";
import FeedTable from "./FeedTable";
import FeedFilter from "./FeedFilter";
import {
  Form,
  Dropdown,
  Grid,
  Segment,
  Button,
  Icon,
  Loader
} from "semantic-ui-react";

class Feed extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {}
  componentDidUpdate(prevProps) {}
  render() {
    return (
      <>
        <FeedFilter />
        <FeedTable />
      </>
    );
  }
}

export default Feed;
