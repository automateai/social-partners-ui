import React, { Component } from "react";
import {
  Button,
  Header,
  Image,
  Modal,
  Grid,
  Form,
  Select,
  Checkbox,
  Radio,
  Input
} from "semantic-ui-react";

const uploadFileStyle = {
  width: "100%",
  height: "133px",
  textAlign: "center",
  color: "#9a9fa7",
  padding: "24px",
  position: "relative",
  display: "inline-flex",
  alignItems: "center",
  borderRadius: "6px",
  border: "1px dashed #dfe3e7",
  cursor: "pointer"
};
const adFormatDropdownDummy = [
  { key: "singleImage", value: "singleImage", text: "Single Image" },
  { key: "snapAds", value: "snapAds", text: "Snap Ads" },
  { key: "carousal", value: "carousal", text: "Carousal" },
  { key: "storyAds", value: "storyAds", text: "Story Ads" },
  { key: "collectionAds", value: "collectionAds", text: "Collection Ads" }
];
class FeedCreateModal extends Component {
  inputStyle = {
    backgroundColor: "#f7f7f7",
    borderRadius: "4px",
    border: "1px solid transparent",
    borderBottom: "1px dashed #289df3",
    boxShadow: "0 0 0 0 transparent",
    color: "#16191c",
    padding: "0 16px 1px",
    height: "40px",
    transition:
      "box-shadow .15s ease-out .15s,border-color .15s,background .15s ease-out .15s,color .15s ease-out"
  };
  handleSubmit = () => {
    console.log("handleSubmit");
  };
  render() {
    return (
      <Modal
        open={this.props.showCreateFeedModal}
        onClose={this.props.handleCreateFeedModal}
      >
        <Modal.Header>Create Feed</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <Form onSubmit={this.handleSubmit}>
              <Grid columns={1}>
                <Grid.Row>
                  <Grid.Column>
                    <Form.Field>
                      <label>Choose a file</label>
                      <div style={uploadFileStyle}>
                        <input type="file" style={{ border: "0" }} />
                      </div>
                    </Form.Field>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
              <Grid columns={2}>
                <Grid.Row>
                  <Grid.Column>
                    <Form.Field>
                      <label>Select Ad Format</label>
                      <Select
                        name="adFormat"
                        style={{
                          backgroundColor: "#f7f7f7",
                          borderRadius: "4px",
                          border: "1px solid transparent",
                          borderBottom: "1px dashed #289df3",
                          boxShadow: "0 0 0 0 transparent",
                          color: "#16191c",
                          height: "40px",
                          transition:
                            "box-shadow .15s ease-out .15s,border-color .15s,background .15s ease-out .15s,color .15s ease-out"
                        }}
                        options={adFormatDropdownDummy}
                      />
                    </Form.Field>
                  </Grid.Column>
                  <Grid.Column>
                    <Form.Field>
                      <label>Select Format</label>
                      <Form.Field>
                        <Radio
                          label="Video"
                          name="radioGroup"
                          value="video"
                          // checked={this.state.value === "this"}
                          // onChange={this.handleChange}
                          style={{ marginRight: 20, marginTop: 10 }}
                        />
                        <Radio
                          label="Image"
                          name="radioGroup"
                          value="image"
                          // checked={this.state.value === "that"}
                          // onChange={this.handleChange}
                        />
                      </Form.Field>
                    </Form.Field>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
              <Grid columns={2}>
                <Grid.Row>
                  <Grid.Column>
                    <Form.Field>
                      <label>No of Ads</label>
                      <input
                        type="number"
                        name="noOfAds"
                        style={this.inputStyle}
                        // value={maxBudget}
                        // onChange={this.handleChange}
                      />
                    </Form.Field>
                  </Grid.Column>
                  <Grid.Column>
                    <Form.Field>
                      <label>No of images per Ad</label>
                      <input
                        type="number"
                        name="noOfImagesPerAd"
                        style={this.inputStyle}
                        // value={maxBudget}
                        // onChange={this.handleChange}
                      />
                    </Form.Field>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
              <Grid columns={2}>
                <Grid.Row>
                  <Grid.Column>
                    <Form.Field>
                      <label>Duration of Video</label>
                      <input
                        type="time"
                        name="videoDuration"
                        style={this.inputStyle}
                        // value={maxBudget}
                        // onChange={this.handleChange}
                      />
                    </Form.Field>
                  </Grid.Column>
                  <Grid.Column>
                    <Form.Field>
                      <label>Transition Time</label>
                      <input
                        type="time"
                        name="transitionTime"
                        style={this.inputStyle}
                        // value={maxBudget}
                        // onChange={this.handleChange}
                      />
                    </Form.Field>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
              <Grid>
                <Grid.Row style={{ display: "flex", justifyContent: "center" }}>
                  <Form.Field>
                    <Button
                      style={{
                        backgroundColor: "#2185d0",
                        color: "white"
                      }}
                      content="Create"
                    />
                  </Form.Field>
                </Grid.Row>
              </Grid>
            </Form>
          </Modal.Description>
        </Modal.Content>
      </Modal>
    );
  }
}
export default FeedCreateModal;
