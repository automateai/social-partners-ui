import React, { Component } from "react";
import {
  Button,
  Header,
  Image,
  Modal,
  List,
  Icon,
  Popup
} from "semantic-ui-react";
import { Player } from "video-react";
import "video-react/dist/video-react.css";
const PreviewList = () => (
  <List divided verticalAlign="middle">
    <List.Item style={{ padding: 20 }}>
      <List.Content floated="right">
        <Icon name="trash" style={{ color: "red", fontSize: 20 }} />
      </List.Content>
      <Popup basic on="click" trigger={<Button icon="video camera" />}>
        <Popup.Content>
          <Player
            playsInline
            poster="/assets/poster.png"
            src={"video/video1.mp4"}
            fluid={false}
            width={250}
            height={400}
          />
        </Popup.Content>
      </Popup>
      <span style={{ marginLeft: 20 }}>Perfume</span>
    </List.Item>
    <List.Item style={{ padding: 20 }}>
      <List.Content floated="right">
        <Icon name="trash" style={{ color: "red", fontSize: 20 }} />
      </List.Content>
      <Popup basic on="click" trigger={<Button icon="video camera" />}>
        <Popup.Content>
          <Player
            playsInline
            poster="/assets/poster.png"
            src={"video/video2.mp4"}
            fluid={false}
            width={250}
            height={400}
          />
        </Popup.Content>
      </Popup>
      <span style={{ marginLeft: 20 }}>Shoe</span>
    </List.Item>
    <List.Item style={{ padding: 20 }}>
      <List.Content floated="right">
        <Icon name="trash" style={{ color: "red", fontSize: 20 }} />
      </List.Content>
      <Popup basic on="click" trigger={<Button icon="video camera" />}>
        <Popup.Content>
          <Player
            playsInline
            poster="/assets/poster.png"
            src={"video/video3.mp4"}
            fluid={false}
            width={250}
            height={400}
          />
        </Popup.Content>
      </Popup>
      <span style={{ marginLeft: 20 }}>Lipstick</span>
    </List.Item>
  </List>
);
class FeedPreviewModal extends Component {
  render() {
    return (
      <Modal
        open={this.props.showPreviewModal}
        onClose={this.props.handlePreviewModal}
      >
        <Modal.Header>Preview</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            <PreviewList />
          </Modal.Description>
        </Modal.Content>
      </Modal>
    );
  }
}
export default FeedPreviewModal;
