import _ from "lodash";
import React, { Component } from "react";
import {
  Table,
  Container,
  Icon,
  Segment,
  Modal,
  Pagination
} from "semantic-ui-react";
import { message } from "antd";
const tableData = [
  {
    channel: "Facebook",
    accountName: "Homesake",
    campaignName: "Homesake_Catalogue_Sales_Automate360",
    adsetName: "IN-18+",
    spends: 210,
    impressions: 21000,
    clicks: 210,
    videoviews: 630,
    installs: 78,
    page_view: 9,
    game_play: 7,
    add_payment_info: 7,
    add_to_basket: 7,
    add_to_wishlist: 3,
    complete_registration: 8,
    contact: 7,
    customise_products: 8,
    donate: 3,
    find_location: 4,
    initiate_checkout: 3,
    lead: 9,
    purchase: 9,
    schedule: 8,
    search: 22,
    start_trial: 3,
    submit_application: 3,
    subscribe: 3,
    view_content: 3,
    cart_value: 1,
    ROI: 1,
    CTR: 0.01,
    CR: 1.29,
    "CR(sale)": 10,
    purchase_Install: 4
  },
  {
    channel: "Facebook",
    accountName: "Homesake",
    campaignName: "Homesake_Catalogue_Sales_Automate361",
    adsetName: "IN-18+",
    spends: 211,
    impressions: 11564,
    clicks: 125,
    videoviews: 630,
    installs: 43,
    page_view: 7,
    game_play: 8,
    add_payment_info: 7,
    add_to_basket: 77,
    add_to_wishlist: 34,
    complete_registration: 1,
    contact: 8,
    customise_products: 1,
    donate: 23,
    find_location: 34,
    initiate_checkout: 1,
    lead: 4,
    purchase: 5,
    schedule: 1,
    search: 21,
    start_trial: 5,
    submit_application: 6,
    subscribe: 3,
    view_content: 9,
    cart_value: 12,
    ROI: 1,
    CTR: 0.01,
    CR: 2.29,
    "CR(sale)": 10,
    purchase_Install: 3
  },
  {
    channel: "Facebook",
    accountName: "Homesake",
    campaignName: "Homesake_Catalogue_Sales_Automate362",
    adsetName: "IN-18+",
    spends: 298,
    impressions: 14257,
    clicks: 176,
    videoviews: 630,
    installs: 9,
    page_view: 1,
    game_play: 1,
    add_payment_info: 3,
    add_to_basket: 7,
    add_to_wishlist: 5,
    complete_registration: 8,
    contact: 12,
    customise_products: 5,
    donate: 4,
    find_location: 8,
    initiate_checkout: 6,
    lead: 6,
    purchase: 2,
    schedule: 9,
    search: 12,
    start_trial: 34,
    submit_application: 7,
    subscribe: 6,
    view_content: 9,
    cart_value: 2,
    ROI: 1,
    CTR: 0.01,
    CR: 3.0,
    "CR(sale)": 10,
    purchase_Install: 6
  },
  {
    channel: "Snapchat",
    accountName: "Homesake",
    campaignName: "Homesake_Catalogue_Sales_Automate362",
    adsetName: "IN-18+",
    spends: 145,
    impressions: 12865,
    clicks: 145,
    videoviews: 630,
    installs: 97,
    page_view: 2,
    game_play: 7,
    add_payment_info: 7,
    add_to_basket: 6,
    add_to_wishlist: 1,
    complete_registration: 1,
    contact: 1,
    customise_products: 1,
    donate: 4,
    find_location: 6,
    initiate_checkout: 5,
    lead: 2,
    purchase: 4,
    schedule: 7,
    search: 10,
    start_trial: 1,
    submit_application: 1,
    subscribe: 1,
    view_content: 2,
    cart_value: 1,
    ROI: 1,
    CTR: 0.01,
    CR: 5.14,
    "CR(sale)": 10,
    purchase_Install: 5
  },
  {
    channel: "Snapchat",
    accountName: "Homesake",
    campaignName: "Homesake_Catalogue_Sales_Automate362",
    adsetName: "IN-18+",
    spends: 156,
    impressions: 14568,
    clicks: 187,
    videoviews: 630,
    installs: 45,
    page_view: 4,
    game_play: 2,
    add_payment_info: 1,
    add_to_basket: 3,
    add_to_wishlist: 7,
    complete_registration: 2,
    contact: 7,
    customise_products: 12,
    donate: 2,
    find_location: 5,
    initiate_checkout: 23,
    lead: 6,
    purchase: 6,
    schedule: 9,
    search: 34,
    start_trial: 7,
    submit_application: 21,
    subscribe: 4,
    view_content: 67,
    cart_value: 8,
    ROI: 1,
    CTR: 0.01,
    CR: 2.78,
    "CR(sale)": 10,
    purchase_Install: 8
  }
];

export default class Table1 extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Segment
        style={{
          margin: "20px 20px 20px 20px",
          maxWidth: 1600,
          overflow: "scroll"
        }}
      >
        <h3>Statistics</h3>
        <Table striped size={"small"}>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell collapsing={false}>Channel</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>
                AccountName
              </Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>
                CampaignName
              </Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>AdsetName</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>Spends</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>
                Impressions
              </Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>Clicks</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>
                Video Views
              </Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>Installs</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>Page View</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>Game Play</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>
                Add Payment Info
              </Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>
                Add To Basket
              </Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>
                Add To Wishlist
              </Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>
                Complete Registration
              </Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>Contact</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>
                Customise Products
              </Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>Donate</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>
                Find Location
              </Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>
                Initiate Checkout
              </Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>Lead</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>Purchase</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>Schedule</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>Search</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>
                Start Trail
              </Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>
                Submit Application
              </Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>Subscribe</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>
                View Content
              </Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>Cart Value</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>ROI</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>CTR</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>CR</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>CR2</Table.HeaderCell>
              <Table.HeaderCell collapsing={false}>
                (Purchase/Install)
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {tableData.length > 0 ? (
              _.map(
                tableData,
                ({
                  channel,
                  accountName,
                  campaignName,
                  adsetName,
                  spends,
                  impressions,
                  clicks,
                  videoviews,
                  installs,
                  page_view,
                  game_play,
                  add_payment_info,
                  add_to_basket,
                  add_to_wishlist,
                  complete_registration,
                  contact,
                  customise_products,
                  donate,
                  find_location,
                  initiate_checkout,
                  lead,
                  purchase,
                  schedule,
                  search,
                  start_trial,
                  submit_application,
                  subscribe,
                  view_content,
                  cart_value,
                  ROI,
                  CTR,
                  CR,
                  CR2,
                  purchase_Install
                }) => (
                  <Table.Row key={Math.random()}>
                    <Table.Cell>{channel}</Table.Cell>
                    <Table.Cell
                      style={{ whiteSpace: "normal", wordBreak: "break-all" }}
                      title={accountName}
                    >
                      {accountName}
                    </Table.Cell>
                    <Table.Cell
                      style={{ whiteSpace: "normal", wordBreak: "break-all" }}
                      title={campaignName}
                      collapsing={true}
                    >
                      {campaignName}
                    </Table.Cell>
                    <Table.Cell
                      style={{ whiteSpace: "normal", wordBreak: "break-all" }}
                      title={adsetName}
                      collapsing={true}
                    >
                      {adsetName}
                    </Table.Cell>
                    <Table.Cell title={spends} collapsing={true}>
                      {spends}
                    </Table.Cell>
                    <Table.Cell title={impressions} collapsing={true}>
                      {impressions}
                    </Table.Cell>
                    <Table.Cell title={clicks} collapsing={true}>
                      {clicks}
                    </Table.Cell>
                    <Table.Cell title={videoviews} collapsing={true}>
                      {videoviews}
                    </Table.Cell>
                    <Table.Cell title={installs} collapsing={true}>
                      {installs}
                    </Table.Cell>
                    <Table.Cell title={page_view} collapsing={true}>
                      {page_view}
                    </Table.Cell>
                    <Table.Cell title={game_play} collapsing={true}>
                      {game_play}
                    </Table.Cell>
                    <Table.Cell title={add_payment_info} collapsing={true}>
                      {add_payment_info}
                    </Table.Cell>
                    <Table.Cell title={add_to_basket} collapsing={true}>
                      {add_to_basket}
                    </Table.Cell>
                    <Table.Cell title={add_to_wishlist} collapsing={true}>
                      {add_to_wishlist}
                    </Table.Cell>
                    <Table.Cell title={complete_registration} collapsing={true}>
                      {complete_registration}
                    </Table.Cell>
                    <Table.Cell title={contact} collapsing={true}>
                      {contact}
                    </Table.Cell>
                    <Table.Cell title={customise_products} collapsing={true}>
                      {customise_products}
                    </Table.Cell>
                    <Table.Cell title={donate} collapsing={true}>
                      {donate}
                    </Table.Cell>
                    <Table.Cell title={find_location} collapsing={true}>
                      {find_location}
                    </Table.Cell>
                    <Table.Cell title={initiate_checkout} collapsing={true}>
                      {initiate_checkout}
                    </Table.Cell>
                    <Table.Cell title={lead} collapsing={true}>
                      {lead}
                    </Table.Cell>
                    <Table.Cell title={purchase} collapsing={true}>
                      {purchase}
                    </Table.Cell>
                    <Table.Cell title={schedule} collapsing={true}>
                      {schedule}
                    </Table.Cell>
                    <Table.Cell title={search} collapsing={true}>
                      {search}
                    </Table.Cell>
                    <Table.Cell title={start_trial} collapsing={true}>
                      {start_trial}
                    </Table.Cell>
                    <Table.Cell title={submit_application} collapsing={true}>
                      {submit_application}
                    </Table.Cell>
                    <Table.Cell title={subscribe} collapsing={true}>
                      {subscribe}
                    </Table.Cell>
                    <Table.Cell title={view_content} collapsing={true}>
                      {view_content}
                    </Table.Cell>
                    <Table.Cell title={cart_value} collapsing={true}>
                      {cart_value}
                    </Table.Cell>
                    <Table.Cell title={ROI} collapsing={true}>
                      {ROI}
                    </Table.Cell>

                    <Table.Cell title={CTR} collapsing={true}>
                      {CTR}
                    </Table.Cell>
                    <Table.Cell title={CR} collapsing={true}>
                      {CR}
                    </Table.Cell>
                    <Table.Cell title={CR2} collapsing={true}>
                      {CR2}
                    </Table.Cell>
                    <Table.Cell title={purchase_Install} collapsing={true}>
                      {purchase_Install}
                    </Table.Cell>
                  </Table.Row>
                )
              )
            ) : (
              <>
                <span
                  style={{
                    height: "250px",
                    display: "inline-block"
                  }}
                ></span>
              </>
            )}
          </Table.Body>
        </Table>
      </Segment>
    );
  }
}
