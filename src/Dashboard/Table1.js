import _ from "lodash";
import React, { Component } from "react";
import {
  Table,
  Container,
  Icon,
  Segment,
  Modal,
  Pagination
} from "semantic-ui-react";
import { message } from "antd";
const tableData = [
  {
    productID: "B016MS1G7O",
    productName: "Homesake A0302 Regal Crystal Antique Table Lamp (Silver)",
    productCatgeory: "Table Lamps",
    cartValue: "20.43",
    purchase: "1",
    ctr: "0.01",
    cr1: "4.29%",
    cr2: "11.11%"
  },
  {
    productID: "B01HTCY6WE",
    productName:
      "Homesake Golden Tree with Bird Nest Votive Stand Yellow, Wall Candle Holder and Tealight Candles",
    productCatgeory: "Table Lamps",
    cartValue: "14.27",
    purchase: "1",
    ctr: "0.01",
    cr1: "4.08%",
    cr2: "5.00%"
  },
  {
    productID: "B01HTCZ35I",
    productName:
      "Homesake™ Black Wire Cruise Three Votive Stand Red, Candle Stand with Candles",
    productCatgeory: "Table Lamps",
    cartValue: "9.99",
    purchase: "1",
    ctr: "0.01",
    cr1: "4.02%",
    cr2: "2.86%"
  },
  {
    productID: "B01HTCZ560",
    productName:
      "Homesake™ Black Wire Cruise Three Votive Stand Yellow, Candle Stand with Candles",
    productCatgeory: "Table Lamps",
    cartValue: "29.96",
    purchase: "3",
    ctr: "0.03",
    cr1: "5.00%",
    cr2: "2.27%"
  },
  {
    productID: "B01HTCZ6CS",
    productName:
      "Homesake Cardinal Red Mercury Silver T-Light Holder, Glass Candle Holder Stand with Free Candle",
    productCatgeory: "Table Lamps",
    purchase: "1",
    cartValue: "20.43",
    ctr: "0.02",
    cr1: "5.00%",
    cr2: "1.06%"
  },
  {
    productID: "B01HTCZ7KO",
    productName:
      "Homesake™ Black Wire Cruise Three Votive Stand Turquoise, Candle Stand with Candles",
    productCatgeory: "Table Lamps",
    cartValue: "9.99",
    purchase: "1",
    ctr: "0.02",
    cr1: "5.00%",
    cr2: "2.78%"
  }
];

export default class Table1 extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Segment style={{ margin: "20px 20px 20px 20px" }}>
        <h3>Product analytics</h3>
        <Table striped size={"small"}>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell width={1} collapsing={false}>
                Product name
              </Table.HeaderCell>
              <Table.HeaderCell width={1} collapsing={false}>
                Product id
              </Table.HeaderCell>
              <Table.HeaderCell width={1} collapsing={false}>
                Purchase
              </Table.HeaderCell>
              <Table.HeaderCell width={1} collapsing={false}>
                Cart value
              </Table.HeaderCell>
              <Table.HeaderCell width={1} collapsing={false}>
                CTR
              </Table.HeaderCell>
              <Table.HeaderCell width={1} collapsing={false}>
                CR (install/click)
              </Table.HeaderCell>
              <Table.HeaderCell width={1} collapsing={false}>
                CR (purchase/install)
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {tableData.length > 0 ? (
              _.map(
                tableData,
                ({
                  productName,
                  productID,
                  purchase,
                  cartValue,
                  ctr,
                  cr1,
                  cr2
                }) => (
                  <Table.Row key={productID + Math.random()}>
                    <Table.Cell
                      style={{ whiteSpace: "normal", wordBreak: "break-all" }}
                    >
                      {productName}
                    </Table.Cell>
                    <Table.Cell
                      style={{ whiteSpace: "normal", wordBreak: "break-all" }}
                      title={productID}
                    >
                      {productID}
                    </Table.Cell>
                    <Table.Cell
                      style={{ whiteSpace: "normal", wordBreak: "break-all" }}
                      title={purchase}
                      collapsing={true}
                    >
                      {purchase}
                    </Table.Cell>
                    <Table.Cell
                      style={{ whiteSpace: "normal", wordBreak: "break-all" }}
                      title={cartValue}
                      collapsing={true}
                    >
                      {cartValue}
                    </Table.Cell>
                    <Table.Cell
                      style={{ whiteSpace: "normal", wordBreak: "break-all" }}
                      title={ctr}
                      collapsing={true}
                    >
                      {ctr}
                    </Table.Cell>
                    <Table.Cell
                      style={{ whiteSpace: "normal", wordBreak: "break-all" }}
                      title={cr1}
                      collapsing={true}
                    >
                      {cr1}
                    </Table.Cell>
                    <Table.Cell
                      style={{ whiteSpace: "normal", wordBreak: "break-all" }}
                      title={cr2}
                      collapsing={true}
                    >
                      {cr2}
                    </Table.Cell>
                  </Table.Row>
                )
              )
            ) : (
              <>
                <span
                  style={{
                    height: "250px",
                    display: "inline-block"
                  }}
                ></span>
              </>
            )}
          </Table.Body>
        </Table>
      </Segment>
    );
  }
}
