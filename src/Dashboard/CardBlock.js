import React, { Component } from "react";
import {
  Form,
  Dropdown,
  Grid,
  Segment,
  Button,
  Icon,
  Loader,
  Select,
  Card,
  Feed
} from "semantic-ui-react";
const Cards = ({ metric, index }) => {
  return (
    <Card style={{ width: 180, height: 120, backgroundColor: "#fafafa" }}>
      <Card.Content>
        <Feed>
          <Feed.Event>
            <Feed.Label>
              <Icon
                disabled
                name={metric[1].icon}
                style={{ color: metric[1].color }}
              />
            </Feed.Label>
            <Feed.Content>
              <Feed.Date
                content={metric[1].value}
                style={{ fontSize: 24, color: "black" }}
              />
              <Feed.Summary style={{ fontSize: 16, marginTop: 10 }}>
                {metric[1].name}
              </Feed.Summary>
            </Feed.Content>
          </Feed.Event>
        </Feed>
      </Card.Content>
      <Card.Content
        extra
        style={{ fontSize: 16, color: "black", textAlign: "center" }}
      >
        {metric[1].percent}{" "}
        <span>
          <Icon
            disabled
            name={metric[1].icon1}
            style={{ color: metric[1].color1 }}
          />
        </span>
      </Card.Content>
    </Card>
  );
};
class CardBlock extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardBlockList: {
        spends: {
          name: "Spends",
          value: "9587",
          percent: "13%",
          icon: "dollar sign",
          icon1: "arrow down",
          color: "#ffa31a",
          color1: "red"
        },
        impressions: {
          name: "Impressions",
          value: "5578",
          percent: "50%",
          icon: "eye",
          icon1: "arrow up",
          color: "red",
          color1: "green"
        },
        clicks: {
          name: "Clicks",
          value: "1000",
          percent: "20%",
          icon: "check",
          icon1: "arrow up",
          color: "blue",
          color1: "green"
        },
        installs: {
          name: "Installs",
          value: "500",
          percent: "10%",
          icon: "google play",
          icon1: "arrow up",
          color: "green",
          color1: "green"
        },
        purchase: {
          name: "Purchase",
          value: "200",
          percent: "5%",
          icon: "cart arrow down",
          icon1: "arrow up",
          color: "#cc0000",
          color1: "green"
        }
      }
    };
  }
  componentDidMount() {}
  componentDidUpdate(prevProps) {}
  render() {
    const { cardBlockList } = this.state;
    return (
      <Segment
        style={{
          margin: "0px 20px 0px 20px"
        }}
      >
        <Grid>
          <Grid.Row style={{ display: "block", padding: 20 }}>
            <div style={{ display: "flex", justifyContent: "space-around" }}>
              {Object.entries(cardBlockList).map((metric, index) => (
                <Grid.Column>
                  <Cards metric={metric} index={index} />
                </Grid.Column>
              ))}
            </div>
          </Grid.Row>
        </Grid>
      </Segment>
    );
  }
}

export default CardBlock;
