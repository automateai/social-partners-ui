import React from "react";
import {
  G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util
} from "bizcharts";
import Total from "./mock/Chart2.js";
class Curved extends React.Component {
  render() {
    const { metric1, metric2 } = this.props;
    const data = Total.map(item => ({
      date: item["Date"],
      metric1: parseInt(item[metric1]),
      metric2: parseInt(item[metric2])
    }));
    return (
      <>
        <div>
          <Chart height={300} width={600} data={data} forceFit padding="auto">
            <Axis name="date" />
            <Axis name="metric1" />
            <Axis name="metric2" />
            <Tooltip
              crosshairs={{
                type: "y"
              }}
            />
            <Geom
              type="line"
              position="date*metric1"
              size={3}
              color={"#03A9F4"}
              shape={"smooth"}
            />
            <Geom type="point" position="date*metric1" size={4} />

            <Geom
              type="line"
              position="date*metric2"
              size={3}
              color={"#E91E63"}
              shape={"smooth"}
            />
            <Geom type="point" position="date*metric2" size={4} />
          </Chart>
        </div>
      </>
    );
  }
}

export default Curved;
