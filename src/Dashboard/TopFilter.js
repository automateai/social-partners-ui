import React, { Component } from "react";
import {
  Form,
  Dropdown,
  Grid,
  Segment,
  Button,
  Icon,
  Loader,
  Select
} from "semantic-ui-react";
const FilterConfig = ["channel", "adAccount", "campaign", "adset", "date"];
const SelectBox = (placeholder, options) => (
  <Select placeholder={placeholder} options={options} />
);
class Feed extends Component {
  constructor(props) {
    super(props);
    this.state = {
      channelData: [
        { key: "fb", text: "Facebook", value: "facebook" },
        { key: "snap", text: "Snapchat", value: "snapchat" }
      ],
      adAccountData: [
        { key: "intellectads", text: "Intellect Ads", value: "intellect ads" },
        {
          key: "hayden&reynott",
          text: "Hayden & Reynott",
          value: "hayden & reynott"
        },
        { key: "automate360", text: "Automate 360", value: "automate360" }
      ],
      campaignData: [
        {
          key: "Automate_Exness_TH_VN_ZA",
          text: "Automate_Exness_TH_VN_ZA",
          value: "Automate_Exness_TH_VN_ZA"
        },
        {
          key: "UKU_ADR_ID_NEW_ACCOUNT",
          text: "UKU_ADR_ID_NEW_ACCOUNT Ads",
          value: "UKU_ADR_ID_NEW_ACCOUNT"
        }
      ],
      adsetData: [
        {
          key: "Automate_Exness_TH_VN_ZA",
          text: "Automate_Exness_TH_VN_ZA",
          value: "Automate_Exness_TH_VN_ZA"
        },
        {
          key: "mashreq_ADR_UAE",
          text: "mashreq_ADR_UAE",
          value: "mashreq_ADR_UAE"
        }
      ],
      dateData: [
        {
          key: "19/12/2019",
          text: "19/12/2019",
          value: "19/12/2019"
        },
        {
          key: "20/12/2019",
          text: "20/12/2019",
          value: "20/12/2019"
        }
      ]
    };
  }
  componentDidMount() {}
  componentDidUpdate(prevProps) {}
  render() {
    const {
      channelData,
      adAccountData,
      campaignData,
      adsetData,
      dateData
    } = this.state;
    return (
      <Segment style={{ padding: 20, margin: 20 }}>
        <Form>
          <Form.Group widths="equal">
            <Form.Select
              clearable
              fluid
              search
              label="Channel"
              options={channelData}
              placeholder="Select.."
            />
            <Form.Select
              clearable
              fluid
              search
              label="Ad Account"
              options={adAccountData}
              placeholder="Select.."
            />
            <Form.Select
              clearable
              search
              label="Campaign"
              options={campaignData}
              placeholder="Select.."
            />
            <Form.Select
              clearable
              search
              label="Adset"
              options={adsetData}
              placeholder="Select.."
            />
            <Form.Select
              clearable
              search
              fluid
              label="Date"
              options={dateData}
              placeholder="Select.."
            />
          </Form.Group>
          <Form.Field style={{ textAlign: "center" }}>
            <Button
              style={{ display: "block", maxidth: "300px", margin: "auto" }}
            >
              Submit
            </Button>
          </Form.Field>
        </Form>
      </Segment>
    );
  }
}

export default Feed;
