import React, { Component } from "react";
import {
  Form,
  Dropdown,
  Grid,
  Segment,
  Button,
  Icon,
  Loader
} from "semantic-ui-react";
import TopFilter from "./TopFilter";
import CardBlock from "./CardBlock";
import Table1 from "./Table1";
import Table2 from "./Table2";
import Chart1Wrapper from "./Chart1Wrapper";
import Chart2 from "./Chart2";
import Chart3 from "./Chart3";

class Feed extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {}
  componentDidUpdate(prevProps) {}
  render() {
    return (
      <Grid>
        <Grid.Row>
          <Grid.Column>
            <TopFilter />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <CardBlock />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Segment style={{ margin: "20px 20px 20px 20px" }}>
              <Chart1Wrapper />
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Segment style={{ margin: "20px 20px 20px 20px" }}>
              <Chart2 />
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Segment style={{ margin: "20px 20px 20px 20px" }}>
              <Chart3 />
            </Segment>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Table1 />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>
            <Table2 />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

export default Feed;
