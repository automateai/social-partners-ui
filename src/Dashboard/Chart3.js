import React from "react";
import {
  G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util
} from "bizcharts";
import DataSet from "@antv/data-set";
import Total from "./mock/Chart2.js";

class Innerlabel extends React.Component {
  render() {
    const { DataView } = DataSet;
    // const data = Total.map(item => ({
    //   category: item["Product Catgeory"],
    //   purchase: item["purchase"]
    // }));
    const data = [
      {
        category: "Table Lamps",
        purchases: 20
      },
      {
        category: "Bulbs",
        purchases: 14
      },
      {
        category: "Board",
        purchases: 10
      },
      {
        category: "Wire",
        purchases: 5
      },
      {
        category: "Table",
        purchases: 12
      }
    ];
    const dv = new DataView();
    dv.source(data).transform({
      type: "percent",
      field: "purchases",
      dimension: "category",
      as: "percent"
    });
    const cols = {
      percent: {
        formatter: val => {
          val = val.toFixed(2) * 100 + "%";
          return val;
        }
      }
    };
    return (
      <>
        <div style={{ fontSize: 18, marginTop: 10, fontWeight: 700 }}>
          Category Wise Analysis
        </div>
        <div>
          <Chart height={500} data={dv} scale={cols} padding="auto" forceFit>
            <Coord type="theta" radius={0.75} />
            <Axis name="percent" />
            <Legend position="bottom" />
            <Tooltip
              showTitle={false}
              itemTpl='<li><span style="background-color:{color};" class="g2-tooltip-marker"></span>{name}: {value}</li>'
            />
            <Geom
              type="intervalStack"
              position="percent"
              color="category"
              tooltip={[
                "category*percent",
                (category, percent) => {
                  percent = percent * 100 + "%";
                  return {
                    name: category,
                    value: percent
                  };
                }
              ]}
              style={{
                lineWidth: 1,
                stroke: "#fff"
              }}
            >
              <Label
                content="percent"
                textStyle={{
                  rotate: 0,
                  shadowBlur: 2,
                  shadowColor: "rgba(0, 0, 0, .45)",
                  fontColor: "white"
                }}
              />
            </Geom>
          </Chart>
        </div>
      </>
    );
  }
}

export default Innerlabel;
