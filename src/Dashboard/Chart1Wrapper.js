import React from "react";
import { Segment, Select, Label } from "semantic-ui-react";
import Chart1 from "./Chart1";
const metrices = [
  { key: "spends", value: "spends", text: "spends" },
  { key: "impressions", value: "impressions", text: "impressions" },
  { key: "clicks", value: "clicks", text: "clicks" },
  { key: "videoviews", value: "videoviews", text: "videoviews" },
  { key: "installs", value: "installs", text: "installs" },
  { key: "page_view", value: "page_view", text: "page_view" },
  { key: "game_play", value: "game_play", text: "game_play" },
  {
    key: "add_payment_info",
    value: "add_payment_info",
    text: "add_payment_info"
  },
  { key: "add_to_basket", value: "add_to_basket", text: "add_to_basket" },
  { key: "add_to_wishlist", value: "add_to_wishlist", text: "add_to_wishlist" },
  {
    key: "complete_registration",
    value: "complete_registration",
    text: "complete_registration"
  },
  { key: "contact", value: "contact", text: "contact" },
  {
    key: "customise_products",
    value: "customise_products",
    text: "customise_products"
  },
  { key: "donate", value: "donate", text: "donate" },
  { key: "find_location", value: "find_location", text: "find_location" },
  {
    key: "initiate_checkout",
    value: "initiate_checkout",
    text: "initiate_checkout"
  },
  { key: "lead", value: "lead", text: "lead" },
  { key: "purchase", value: "purchase", text: "purchase" },
  { key: "schedule", value: "schedule", text: "schedule" },
  { key: "search", value: "search", text: "search" },
  { key: "start_trial", value: "start_trial", text: "start_trial" },
  {
    key: "submit_application",
    value: "submit_application",
    text: "submit_application"
  },
  { key: "subscribe", value: "subscribe", text: "subscribe" },
  { key: "view_content", value: "view_content", text: "view_content" },
  { key: "cart_value", value: "cart_value", text: "cart_value" },
  { key: "CTR", value: "CTR", text: "CTR" },
  { key: "CR", value: "CR", text: "CR" },
  { key: "CR- sale", value: "CR- sale", text: "CR- sale" },
  {
    key: "Purchase/Install",
    value: "Purchase/Install",
    text: "Purchase/Install"
  },
  { key: "ROI", value: "ROI", text: "ROI" }
];

class Chart1Wrapper extends React.Component {
  state = { metric1: "spends", metric2: "installs" };

  handleChange = (e, { name, value }) => this.setState({ [name]: value });
  render() {
    let { metric1, metric2 } = this.state;
    return (
      <>
        <div style={{ padding: 25 }}>
          <Label
            style={{
              marginRight: 5,
              color: "#03A9F4",
              backgroundColor: "white"
            }}
          >
            Metric 1
          </Label>
          <Select
            name={"metric1"}
            options={metrices}
            value={metric1}
            onChange={this.handleChange}
            style={{
              marginRight: 20
            }}
          />
          <Label
            style={{
              marginRight: 5,
              color: "#E91E63",
              backgroundColor: "white"
            }}
          >
            Metric 2
          </Label>
          <Select
            name={"metric2"}
            options={metrices}
            value={metric2}
            onChange={this.handleChange}
            style={{ marginRight: 10 }}
          />
        </div>
        <Chart1 metric1={metric1} metric2={metric2} />
      </>
    );
  }
}

export default Chart1Wrapper;
