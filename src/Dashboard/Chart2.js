import React from "react";
import {
  G2,
  Chart,
  Geom,
  Axis,
  Tooltip,
  Coord,
  Label,
  Legend,
  View,
  Guide,
  Shape,
  Facet,
  Util
} from "bizcharts";

import Total from "./mock/Chart2.js";
class Chart2 extends React.Component {
  render() {
    const TableData = Total.map(item => ({
      channel: item.channel,
      installs: item.installs,
      spends: item.spends,
      impressions: item.impressions
    }));
    const cols = {
      installs: {
        type: "pow",
        alias: "installs"
      },
      spends: {
        alias: "spends"
      },
      impressions: {
        alias: "impressions"
      },
      channel: {
        alias: "channel"
      }
    };

    return (
      <>
        <div style={{ fontSize: 18, marginTop: 10, fontWeight: 700 }}>
          Channel Wise Analysis
        </div>
        <div>
          <Chart
            style={{ marginTop: 40 }}
            data={TableData.sort((a, b) =>
              a.impressions > b.impressions ? 1 : -1
            )}
            scale={cols}
            forceFit
            padding="auto"
          >
            <Tooltip showTitle={false} />
            <Axis name="spends" />
            <Axis
              name="impressions"
              label={{
                formatter: value => {
                  return (value / 1000).toFixed(0) + "k";
                }
              }}
            />
            <Legend reversed />
            <Geom
              type="point"
              position="spends*impressions"
              color={["channel"]}
              color={[
                "channel",
                value => {
                  if (value === "Facebook") {
                    return "#3b5998";
                  }
                  if (value === "Snapchat") {
                    return "#FFC107";
                  } else {
                    return "#ec407a";
                  }
                }
              ]}
              tooltip="channel*spends*impressions*installs"
              opacity={0.65}
              shape="circle"
              size={["installs", [4, 65]]}
              style={[
                "channel",
                {
                  lineWidth: 1,
                  strokeOpacity: 1,
                  fillOpacity: 0.3,
                  opacity: 0.65
                }
              ]}
            />
          </Chart>
        </div>
      </>
    );
  }
}

export default Chart2;
