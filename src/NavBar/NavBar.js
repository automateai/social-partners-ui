import React from "react";
import { Button, Menu, Segment, Icon, Grid } from "semantic-ui-react";
import { Redirect, Link } from "react-router-dom";
import LinkAccount from "../linkAccount";
import Cookies from "universal-cookie";
const cookies = new Cookies();
const colorsA = [
  "#289df3",
  "#289df3",
  "#289df3",
  "#289df3",
  "#289df3",
  "#289df3"
];
class NavBar extends React.Component {
  state = {
    activeItem: /*localStorage.getItem("activeItem") || */ "dashboard",
    linkModal: false
  };
  signout = () => {
    localStorage.clear();
    cookies.remove("loggedIn");
    window.location = "/pages/login";
  };
  handleItemClick = (e, { name }) => {
    console.log(name);
    localStorage.setItem("activeItem", name);
    this.setState({ activeItem: name });
    if (name === "rulelist") {
      this.props.history.push("/rulelist");
    } else if (name === "adset level config") {
      this.props.history.push("/adsetruleconfig");
    } else if (name === "ads level config") {
      this.props.history.push("/adsruleconfig");
    } else if (name === "feed") {
      this.props.history.push("/feed");
    } else if (name === "dashboard") {
      this.props.history.push("/dashboard");
    }
  };
  updateModalState = () => {
    let { linkModal } = this.state;
    this.setState({
      linkModal: false
    });
  };
  render() {
    const { activeItem, linkModal } = this.state;
    return (
      <div>
        <LinkAccount
          linkModal={linkModal}
          updateModalState={this.updateModalState}
        />
        <div
          style={{
            backgroundColor: "#20232a",
            height: 60,
            padding: 10,
            top: 0,
            width: "100%"
          }}
        >
          <Grid>
            <Grid.Row>
              <Grid.Column width={16}>
                <Menu inverted secondary>
                  <Menu.Item
                    name="dashboard"
                    active={activeItem === "dashboard"}
                    onClick={this.handleItemClick}
                    color={"blue"}
                  />
                  <Menu.Item
                    name="rulelist"
                    active={activeItem === "rulelist"}
                    onClick={this.handleItemClick}
                    color={"blue"}
                  />
                  <Menu.Item
                    name="adset level config"
                    active={activeItem === "adset level config"}
                    onClick={this.handleItemClick}
                    color={"blue"}
                  />
                  <Menu.Item
                    name="ads level config"
                    active={activeItem === "ads level config"}
                    onClick={this.handleItemClick}
                    color={"blue"}
                  />
                  <Menu.Item
                    name="feed"
                    active={activeItem === "feed"}
                    onClick={this.handleItemClick}
                    color={"blue"}
                  />

                  <Menu.Menu position="right">
                    <Button
                      style={{
                        width: "120px",
                        height: "36px",
                        marginTop: "3px",
                        fontSize: 13,
                        backgroundColor: "#2185d0",
                        borderColor: "#2185d0",
                        color: "white"
                      }}
                      animated="horizontal"
                      floated="right"
                      onClick={() => {
                        this.setState({ linkModal: true });
                      }}
                    >
                      <Button.Content hidden>
                        <Icon
                          name="add user"
                          style={{ fontSize: 15, color: "white" }}
                        />
                      </Button.Content>
                      <Button.Content visible>Link Account</Button.Content>
                    </Button>
                  </Menu.Menu>
                  <Menu.Menu>
                    <Button
                      style={{
                        width: "90px",
                        height: "36px",
                        marginTop: "3px",
                        marginLeft: "10px",
                        color: "white",
                        backgroundColor: "#2185d0"
                      }}
                      onClick={this.signout}
                    >
                      <Button.Content visible>Logout</Button.Content>
                    </Button>
                  </Menu.Menu>
                </Menu>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        </div>
      </div>
    );
  }
}

export default NavBar;
