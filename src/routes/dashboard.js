import RuleList from "../Rules/RuleList";
import AdsetConfig from "../Rules/AdsetConfig";
import AdsConfig from "../Rules/AdsConfig";
import Dashboard from "../Dashboard/index";
import Feed from "../Feed/index";

var dashRoutes = [
  {
    path: "/rulelist",
    name: "Rule List",
    component: RuleList
  },
  {
    path: "/adsetruleconfig",
    name: "Adset Config",
    component: AdsetConfig
  },
  {
    path: "/adsruleconfig",
    name: "Ads Config",
    component: AdsConfig
  },
  {
    path: "/feed",
    name: "Feed",
    component: Feed
  },
  {
    path: "/dashboard",
    name: "Dashboard",
    component: Dashboard
  },
  { redirect: true, path: "/", pathTo: "/rulelist", name: "Rule List" }
];

export default dashRoutes;
