import Pages from "../layout/LoginPage";
import Dashboard from "../layout/Dashboard";

var indexRoutes = [
  { path: "/pages", name: "Pages", component: Pages },
  { path: "/", name: "Home", component: Dashboard }
];

export default indexRoutes;
