import Authentication from "../Authentication/LoginPage";

const pagesRoutes = [
  {
    path: "/pages/login",
    name: "Login Page",
    component: Authentication
  },
  {
    redirect: true,
    path: "/pages",
    pathTo: "/pages/login",
    name: "Login Page"
  }
];

export default pagesRoutes;
