import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import "antd/dist/antd.css";
import App from "./App.js";
import * as serviceWorker from "./serviceWorker";
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import indexRoutes from "./routes/index.js";
import Cookies from "universal-cookie";

const hist = createBrowserHistory();
const cookies = new Cookies();

function isLoggedIn() {
  if (cookies.get("loggedIn")) {
    return true;
  }
  return false;
}

ReactDOM.render(
  <Router history={hist}>
    <Switch>
      {indexRoutes.map((prop, key) => {
        if (isLoggedIn()) {
          prop = indexRoutes.filter(route => route.path === "/").pop();
          return (
            <Route path={prop.path} component={prop.component} key={key} />
          );
        } else {
          prop = indexRoutes.filter(route => route.path === "/pages").pop();
          hist.replace(prop.path);
          return (
            <Route path={prop.path} component={prop.component} key={key} />
          );
        }
      })}
    </Switch>
  </Router>,
  document.getElementById("root")
);

serviceWorker.unregister();
