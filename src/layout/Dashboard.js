import dashboardRoutes from "../routes/dashboard.js";
import NavBar from "../NavBar/NavBar.js";
import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

const switchRoutes = (
  <Switch>
    {dashboardRoutes.map((prop, key) => {
      if (prop.redirect)
        return <Redirect from={prop.path} to={prop.pathTo} key={key} />;
      if (prop.collapse)
        return prop.views.map((prop, key) => {
          return (
            <Route path={prop.path} component={prop.component} key={key} />
          );
        });
      return <Route path={prop.path} component={prop.component} key={key} />;
    })}
  </Switch>
);

class Dashboard extends React.Component {
  render() {
    return (
      <div>
        <NavBar {...this.props} />
        {switchRoutes}
      </div>
    );
  }
}

export default Dashboard;
