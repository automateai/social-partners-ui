import React, { Component } from "react";
import {
  Button,
  Form,
  Segment,
  Container,
  Icon,
  Message,
  Popup,
  Grid
} from "semantic-ui-react";
import ReactTooltip from "react-tooltip";
import Logo from "../assests/logo/logo.svg";
import GlobalElement from "../config/utility.js";
import Cookies from "universal-cookie";
import ReqRespHandler from "../config/ReqRespHandler.js";
const cookies = new Cookies();
class Authentication extends Component {
  state = {
    email: "",
    password: "",
    showModal: false,
    loginError: false,
    emptyError: false,
    loadingLogin: false
  };

  handleEmail = e => {
    this.setState({ email: e.target.value });
  };

  handlePassword = e => {
    this.setState({ password: e.target.value });
  };

  handleSubmit = async () => {
    const { password, email, loginError, loadingLogin } = this.state;
    if (email && password && email !== "" && password !== "") {
      this.setState({ emptyError: false });
      const data = {
        email: email,
        password: password
      };

      this.setState({ loadingLogin: true });
      ReqRespHandler.postCall("loginAPI", false, data, result => {
        if (result[1].success) {
          localStorage.setItem("userData", JSON.stringify(result[1].data[0]));
          cookies.set("loggedIn", "true", { path: "/" });
          this.setState({
            loadingLogin: false,
            loginError: false
          });
          window.location = "/dashboard";
        } else {
          this.setState({
            loadingLogin: false,
            loginError: true
          });
        }
      });
    } else {
      this.setState({ loginError: false, emptyError: true });
    }
  };

  checkKey = e => {
    if (e.key === "Enter") {
      this.handleSubmit();
    }
  };

  componentDidMount() {}

  render() {
    let { email, password, loginError, loadingLogin, emptyError } = this.state;
    return (
      <Grid>
        <Grid.Row stretched style={{ padding: 0 }}>
          <Grid.Column style={{ height: 710 }} width={6} floated={"left"}>
            <img
              src={Logo}
              style={{
                padding: 40
              }}
            />
          </Grid.Column>
          <Grid.Column
            width={10}
            color={"blue"}
            floated={"right"}
            style={{
              height: "725px",
              position: "relative",
              fontFamily: "Avenir Roman"
            }}
          >
            <Segment
              style={{
                position: "absolute",
                top: "15%",
                left: "22%",
                height: "auto",
                minHeight: "500px",
                minWidth: "400px",
                maxWidth: "570px",
                padding: 30,
                boxShadow: "0px 0px 0px 0px"
              }}
            >
              <div style={{ minHeight: "45px" }}>
                {emptyError && (
                  <Message
                    color="yellow"
                    header="Please input Email/Password combination."
                  />
                )}
                {loginError && (
                  <Message
                    color="red"
                    header="Wrong Credentials! Please verify input."
                  />
                )}
              </div>
              <div
                style={{
                  fontSize: "16px",
                  maxHeight: "30px",
                  color: "gray",
                  marginTop: 90,
                  marginBottom: 10
                }}
              >
                Welcome to Automate 360
              </div>
              <div
                style={{
                  fontSize: "40px",
                  maxHeight: "auto",
                  marginTop: 10,
                  marginBottom: 60,
                  lineHeight: 1,
                  color: "#2085d0"
                }}
              >
                Log into your Account
              </div>
              <div
                style={{
                  borderRadius: 0
                }}
              >
                <Form>
                  <Form.Field>
                    <label
                      style={{
                        fontSize: 16,
                        textAlign: "left"
                      }}
                    >
                      Email
                    </label>
                    <input
                      name="email"
                      placeholder="Please enter email"
                      value={email}
                      onChange={this.handleEmail}
                    />
                  </Form.Field>
                  <Form.Field>
                    <label
                      style={{
                        fontSize: 16,
                        textAlign: "left"
                      }}
                    >
                      Password
                    </label>
                    <input
                      name="password"
                      type="Password"
                      placeholder="Please enter password"
                      value={password}
                      onChange={this.handlePassword}
                      onKeyDown={e => {
                        this.checkKey(e);
                      }}
                    />
                  </Form.Field>
                  <Form.Field style={{ textAlign: "center" }}>
                    <Button
                      name="arrow circle right"
                      style={{
                        cursor: "pointer",
                        marginTop: 10,
                        backgroundColor: "#2085d0",
                        color: "white",
                        borderRadius: 2
                      }}
                      onClick={this.handleSubmit}
                      loading={loadingLogin}
                    >
                      Login
                    </Button>
                  </Form.Field>
                </Form>
              </div>
            </Segment>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

export default Authentication;
