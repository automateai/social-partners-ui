import GlobalElement from "../config/utility.js";
const apiEndpoints = {
  loginAPI: "/login",
  accountList: "/account",
  publisherList: "/publishers",
  addAccount: "/account",
  generateRuleId: "/rule"
};

const ReqRespHandler = {
  postCall: async (endPoint, isEndPointSet, data, _callback) => {
    const URL = isEndPointSet
      ? `${GlobalElement.URL}` + endPoint
      : `${GlobalElement.URL}` + apiEndpoints[endPoint];
    const response = await fetch(URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)
    })
      .then(async response => {
        _callback([true, await response.json()]);
      })
      .catch(err => {
        if (err) {
          _callback([false, err]);
        }
      });
  },

  postAuthCall: async (endPoint, isEndPointSet, data, _callback) => {
    const userData = localStorage.getItem("userData");
    const URL = isEndPointSet
      ? `${GlobalElement.URL}` + endPoint
      : `${GlobalElement.URL}` + apiEndpoints[endPoint];
    const response = await fetch(URL, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: JSON.parse(userData).token
      },
      body: JSON.stringify(data)
    })
      .then(async response => {
        _callback([true, await response.json()]);
      })
      .catch(err => {
        if (err) {
          _callback([false, err]);
        }
      });
  },

  getCall: async (endPoint, isEndPointSet, _callback) => {
    const userData = localStorage.getItem("userData");
    const URL = isEndPointSet
      ? `${GlobalElement.URL}` + endPoint
      : `${GlobalElement.URL}` + apiEndpoints[endPoint];
    const response = await fetch(URL, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: JSON.parse(userData).token
      }
    })
      .then(async response => {
        _callback([true, await response.json()]);
      })
      .catch(err => {
        if (err) {
          _callback([false, err]);
        }
      });
  },

  putCall: async (endPoint, isEndPointSet, data, _callback) => {
    const userData = localStorage.getItem("userData");
    const URL = isEndPointSet
      ? `${GlobalElement.URL}` + endPoint
      : `${GlobalElement.URL}` + apiEndpoints[endPoint];
    const response = await fetch(URL, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: JSON.parse(userData).token
      },
      body: JSON.stringify(data)
    })
      .then(async response => {
        _callback([true, await response.json()]);
      })
      .catch(err => {
        if (err) {
          _callback([false, err]);
        }
      });
  }
};

export default ReqRespHandler;
