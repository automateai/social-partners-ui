const Mapping = {
  targetEvent: {
    spends: {
      name: "Spends"
    },
    impressions: {
      name: "Impressions"
    },
    clicks: {
      name: "Clicks"
    },
    videoviews: {
      name: "Videoviews"
    },
    installs: {
      name: "Installs"
    },
    page_view: {
      name: "Page View"
    },
    game_play: {
      name: "Game Play"
    },
    add_payment_info: {
      name: "Add Payment Info"
    },
    add_to_basket: {
      name: "Add To Basket"
    },
    add_to_wishlist: {
      name: "Add To Wishlist"
    },
    complete_registration: {
      name: "Complete Registration"
    },
    initiate_checkout: {
      name: "Initiate Checkout"
    },
    lead: {
      name: "Lead"
    },
    purchase: {
      name: "Purchase"
    },
    search: {
      name: "Search"
    },
    submit_application: {
      name: "Submit Application"
    },
    subscribe: {
      name: "Subscribe"
    },
    view_content: {
      name: "View Content"
    },
    cart_value: {
      name: "Cart Value"
    }
  },
  viewAttributionWindow: {
    "28_day": {
      name: "28 Days"
    },
    "7_day": {
      name: "7 Days"
    },
    "1_day": {
      name: "1 Days"
    },
    none: {
      name: "None"
    }
  },
  swipeUpAttributionWindow: {
    "28_day": {
      name: "28 Days"
    },
    "7_day": {
      name: "7 Days"
    },
    "1_day": {
      name: "1 Day"
    }
  }
};
export default Mapping;
