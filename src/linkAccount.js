import React from "react";
import {
  Modal,
  Form,
  Grid,
  Dropdown,
  Segment,
  Menu,
  Icon,
  Input,
  Button,
  Checkbox,
  Message
} from "semantic-ui-react";
import ReqRespHandler from "./config/ReqRespHandler.js";
const labelStyle = {
  color: "#289df3"
};
const inputStyle = {
  backgroundColor: "#f7f7f7",
  borderRadius: "4px",
  border: "1px solid transparent",
  boxShadow: "0 0 0 0 transparent",
  color: "#16191c",
  height: "40px",
  transition:
    "box-shadow .15s ease-out .15s,border-color .15s,background .15s ease-out .15s,color .15s ease-out"
};

class linkAccount extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      publisherList: [],
      inputAccountID: "",
      inputAccountName: "",
      inputPublisher: "",
      accountStatus: {
        status: false,
        msg: "",
        color: ""
      }
    };
  }

  componentDidMount = () => {
    let { publisherList } = this.state;
    const userData = localStorage.getItem("userData");

    //...getting publishers
    ReqRespHandler.getCall("publisherList", false, result => {
      if (result[1].success) {
        const publisherResponse = result[1].data;
        const publisherList = publisherResponse.map(function(publisher, key) {
          return {
            key: key,
            value: publisher.publisher_id,
            text: publisher.name
          };
        });
        this.setState({
          publisherList: publisherList
        });
      } else {
        this.setState({
          publisherList: []
        });
      }
    });
  };

  AccountStatusMessage = () => (
    <Message compact color={this.state.accountStatus["color"]}>
      {this.state.accountStatus["msg"]}
    </Message>
  );

  createAccount = () => {
    let {
      inputAccountID,
      inputAccountName,
      inputPublisher,
      accountStatus
    } = this.state;
    const data = {
      publisher_id: inputPublisher,
      pub_account_id: inputAccountID,
      name: inputAccountName
    };

    ReqRespHandler.postAuthCall("addAccount", false, data, result => {
      if (result[1].success) {
        accountStatus["status"] = true;
        accountStatus["msg"] = "Account Created Successfully!";
        accountStatus["color"] = "green";
        this.setState({
          inputAccountID: "",
          inputAccountName: "",
          inputPublisher: "",
          accountStatus
        });
      } else {
        accountStatus["status"] = true;
        accountStatus["msg"] = "Something went wrong! Try Again!";
        accountStatus["color"] = "red";
        this.setState({
          accountStatus
        });
      }
    });
  };
  render() {
    let {
      publisherList,
      inputAccountID,
      inputAccountName,
      inputPublisher,
      accountStatus
    } = this.state;
    return (
      <>
        <Modal
          open={this.props.linkModal}
          onClose={this.props.updateModalState}
        >
          <Modal.Header>Link Account</Modal.Header>
          <Modal.Content>
            <Modal.Description>
              <Grid>
                <Grid.Row>
                  <Grid.Column style={{ textAlign: "center" }}>
                    {accountStatus["status"] && <this.AccountStatusMessage />}
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                  <Grid.Column
                    style={{
                      textAlign: "center",
                      fontSize: "24px",
                      fontWeight: 400
                    }}
                  >
                    Enter details to Add an Account
                  </Grid.Column>
                </Grid.Row>
                <Grid.Row style={{ padding: 0 }}>
                  <Grid.Column style={{ padding: 20 }}>
                    <Form>
                      <Form.Field>
                        <label style={labelStyle}>Account Name</label>
                        <input
                          style={inputStyle}
                          placeholder="Enter Account Name"
                          value={inputAccountName}
                          onChange={e => {
                            this.setState({ inputAccountName: e.target.value });
                          }}
                        />
                      </Form.Field>
                      <Form.Field>
                        <label style={labelStyle}>Account ID</label>
                        <input
                          style={inputStyle}
                          placeholder="Enter Account ID"
                          value={inputAccountID}
                          onChange={e => {
                            this.setState({ inputAccountID: e.target.value });
                          }}
                        />
                      </Form.Field>
                      <Form.Field>
                        <label style={labelStyle}>Publisher</label>
                        <Dropdown
                          placeholder="Select Publisher"
                          fluid
                          clearable
                          search
                          selection
                          options={publisherList}
                          style={inputStyle}
                          value={inputPublisher}
                          onChange={(e, { value }) => {
                            this.setState({ inputPublisher: value });
                          }}
                        />
                      </Form.Field>

                      <Form.Field>
                        <Button
                          type="submit"
                          disabled={
                            inputAccountID === "" ||
                            inputAccountName === "" ||
                            inputPublisher === ""
                          }
                          style={{
                            float: "right",
                            margin: 10,
                            color: "white",
                            backgroundColor: "#289df3"
                          }}
                          onClick={this.createAccount}
                        >
                          Submit
                        </Button>
                      </Form.Field>
                    </Form>
                  </Grid.Column>
                </Grid.Row>
              </Grid>
            </Modal.Description>
          </Modal.Content>
        </Modal>
      </>
    );
  }
}
export default linkAccount;
