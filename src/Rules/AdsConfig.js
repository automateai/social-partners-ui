import React, { Component } from "react";
import {
  Form,
  Dropdown,
  Grid,
  Segment,
  Button,
  Select,
  Checkbox,
  Table,
  Icon,
  Label,
  Menu
} from "semantic-ui-react";

class AdsetConfig extends Component {
  inputStyle = {
    backgroundColor: "#f7f7f7",
    borderRadius: "4px",
    border: "1px solid transparent",
    boxShadow: "0 0 0 0 transparent",
    color: "#16191c",
    padding: "0 16px 1px",
    height: "40px",
    transition:
      "box-shadow .15s ease-out .15s,border-color .15s,background .15s ease-out .15s,color .15s ease-out"
  };
  render() {
    return (
      <Grid>
        <Grid.Row>
          <Grid.Column style={{ padding: 50 }}>
            <Segment
              className={"w3-animate-bottom"}
              style={{
                borderRadius: 0,
                textAlign: "left"
              }}
            >
              <Grid>
                <Grid.Row style={{ padding: 0 }}>
                  <Table singleLine>
                    <Table.Header>
                      <Table.Row>
                        <Table.HeaderCell>Ad Name</Table.HeaderCell>
                        <Table.HeaderCell>Status</Table.HeaderCell>
                        <Table.HeaderCell>Optimization</Table.HeaderCell>
                        <Table.HeaderCell>Minimum Budget</Table.HeaderCell>
                        <Table.HeaderCell>Maximum Budget</Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                    <Table.Body>
                      <Table.Row>
                        <Table.Cell>Miss Jockey - Ad</Table.Cell>
                        <Table.Cell>
                          <Checkbox toggle />
                        </Table.Cell>
                        <Table.Cell>
                          <Checkbox toggle />
                        </Table.Cell>
                        <Table.Cell>
                          <input />
                        </Table.Cell>
                        <Table.Cell>
                          <input />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>
                          2019-05-31 11:05:18_Ecommerce_-1_1|2|3|4|5|6|7|8
                        </Table.Cell>
                        <Table.Cell>
                          <Checkbox toggle />
                        </Table.Cell>
                        <Table.Cell>
                          <Checkbox toggle />
                        </Table.Cell>
                        <Table.Cell>
                          <input />
                        </Table.Cell>
                        <Table.Cell>
                          <input />
                        </Table.Cell>
                      </Table.Row>
                      <Table.Row>
                        <Table.Cell>
                          2019-05-31 11:05:18_Ecommerce_-1_1|2
                        </Table.Cell>
                        <Table.Cell>
                          <Checkbox toggle />
                        </Table.Cell>
                        <Table.Cell>
                          <Checkbox toggle />
                        </Table.Cell>
                        <Table.Cell>
                          <input />
                        </Table.Cell>
                        <Table.Cell>
                          <input />
                        </Table.Cell>
                      </Table.Row>
                    </Table.Body>
                    <Table.Footer>
                      <Table.Row>
                        <Table.HeaderCell colSpan="5">
                          <Menu floated="right" pagination>
                            <Menu.Item as="a" icon>
                              <Icon name="chevron left" />
                            </Menu.Item>
                            <Menu.Item as="a">1</Menu.Item>
                            <Menu.Item as="a">2</Menu.Item>
                            <Menu.Item as="a">3</Menu.Item>
                            <Menu.Item as="a">4</Menu.Item>
                            <Menu.Item as="a" icon>
                              <Icon name="chevron right" />
                            </Menu.Item>
                          </Menu>
                        </Table.HeaderCell>
                      </Table.Row>
                    </Table.Footer>
                  </Table>
                </Grid.Row>
              </Grid>
            </Segment>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

export default AdsetConfig;
