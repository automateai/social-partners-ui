import React from "react";
import { Modal, Form, Grid, Select, Checkbox, Button } from "semantic-ui-react";
import { isEqual } from "lodash";
import ReqRespHandler from "../config/ReqRespHandler.js";
import Mapping from "../config/Mapping.js";
import { message } from "antd";
const targetFields = Object.entries(Mapping.targetEvent).map(function(obj) {
  return { key: obj[0], value: obj[0], text: obj[1].name };
});
const viewFields = Object.entries(Mapping.viewAttributionWindow).map(function(
  obj
) {
  return { key: obj[0], value: obj[0], text: obj[1].name };
});
const swipeUpFields = Object.entries(Mapping.swipeUpAttributionWindow).map(
  function(obj) {
    return { key: obj[0], value: obj[0], text: obj[1].name };
  }
);
class AdsetConfigModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      targetField: "installs",
      targetCPA: "",
      activeMultipleAds: 0,
      adLevelBudget: 0,
      manualAdRotation: 0,
      minBudget: "",
      maxBudget: "",
      viewAttr: "28_day",
      swipeAttr: "28_day",
      cpaCalcDays: "",
      activeAds: "",
      newRuleId: "",
      targetFieldsMapping: targetFields,
      viewAttrFieldMapping: viewFields,
      swipeUpFieldMapping: swipeUpFields,
      blank: true
    };
  }
  inputStyle = {
    backgroundColor: "#f7f7f7",
    borderRadius: "4px",
    border: "1px solid transparent",
    borderBottom: "1px dashed #289df3",
    boxShadow: "0 0 0 0 transparent",
    color: "#16191c",
    padding: "0 16px 1px",
    height: "40px",
    transition:
      "box-shadow .15s ease-out .15s,border-color .15s,background .15s ease-out .15s,color .15s ease-out"
  };

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    let { data, newGenRuleData } = this.props;
    let {
      targetField,
      targetCPA,
      activeMultipleAds,
      adLevelBudget,
      manualAdRotation,
      minBudget,
      maxBudget,
      viewAttr,
      swipeAttr,
      cpaCalcDays,
      activeAds,
      blank
    } = this.state;
    if (data && data.length > 0 && !isEqual(data[0], prevProps.data[0])) {
      this.setState({
        targetField: data[0].cpa_target_event,
        targetCPA: data[0].cpa_target_value,
        activeMultipleAds: data[0].is_multiple_ad,
        adLevelBudget: data[0].is_ad_level_budget,
        manualAdRotation: data[0].manual_adrotate_ad_not_in_optimization,
        minBudget: data[0].min_budget,
        maxBudget: data[0].max_budget,
        viewAttr: data[0].view_attribution_window,
        swipeAttr: data[0].swipe_up_attribution_window,
        cpaCalcDays: data[0].cpa_calculation_days,
        activeAds: data[0].number_of_ads
      });
    }
  };

  handleChange = event => {
    const {
      target: { name, value }
    } = event;
    this.setState({ [name]: value });
  };

  handleSelectChange = (e, { value, name }) => {
    this.setState({ [name]: value });
  };

  handleCheckbox = (name, value) => {
    this.setState({ [name]: value });
  };

  handleSubmit = () => {
    let {
      targetField,
      targetCPA,
      activeMultipleAds,
      adLevelBudget,
      manualAdRotation,
      minBudget,
      maxBudget,
      viewAttr,
      swipeAttr,
      cpaCalcDays,
      activeAds
    } = this.state;
    let { selectedAdset } = this.props;
    let ruleData = {
      cpa_target_value: targetCPA,
      cpa_target_event: targetField
    };

    if (activeMultipleAds !== "")
      ruleData["is_multiple_ad"] = activeMultipleAds;
    if (adLevelBudget !== "") ruleData["is_ad_level_budget"] = adLevelBudget;
    if (manualAdRotation !== "")
      ruleData["manual_adrotate_ad_not_in_optimization"] = manualAdRotation;
    if (minBudget !== "") ruleData["min_budget"] = parseInt(minBudget);
    if (maxBudget !== "") ruleData["max_budget"] = parseInt(maxBudget);
    if (viewAttr !== "") ruleData["view_attribution_window"] = viewAttr;
    if (swipeAttr !== "") ruleData["swipe_up_attribution_window"] = swipeAttr;
    if (cpaCalcDays !== "")
      ruleData["cpa_calculation_days"] = parseInt(cpaCalcDays);
    if (activeAds !== "") ruleData["number_of_ads"] = parseInt(activeAds);

    ReqRespHandler.postAuthCall("generateRuleId", false, ruleData, result => {
      if (result[1].success) {
        let adsetSelected = [];
        adsetSelected.push(selectedAdset);
        let data = {
          adset_list: adsetSelected,
          rule_id: result[1].data[0].rule_id
        };
        ReqRespHandler.putCall(`/adset`, true, data, result => {
          if (result[1].success) {
            message.success("Successfully Saved!");
            this.props.updateModalState();
            this.props.refetchTableData();
          } else {
            console.log("error.....");
          }
        });
      } else {
        console.log("Something went wrong!");
      }
    });
  };

  onModalClose = () => {
    let {
      targetField,
      targetCPA,
      activeMultipleAds,
      adLevelBudget,
      manualAdRotation,
      minBudget,
      maxBudget,
      viewAttr,
      swipeAttr,
      cpaCalcDays,
      activeAds
    } = this.state;
    this.setState({
      targetField: targetField,
      targetCPA: "",
      activeMultipleAds: 0,
      adLevelBudget: 0,
      manualAdRotation: 0,
      minBudget: "",
      maxBudget: "",
      viewAttr: viewAttr,
      swipeAttr: swipeAttr,
      cpaCalcDays: "",
      activeAds: ""
    });
    this.props.updateAdsetStatus();
  };
  render() {
    let { data, newGeneratedRuleData, ruleID } = this.props;
    let {
      targetField,
      targetCPA,
      activeMultipleAds,
      adLevelBudget,
      manualAdRotation,
      minBudget,
      maxBudget,
      viewAttr,
      swipeAttr,
      cpaCalcDays,
      activeAds,
      targetFieldsMapping,
      viewAttrFieldMapping,
      swipeUpFieldMapping
    } = this.state;
    return (
      <>
        <Modal open={this.props.showModal} onClose={this.onModalClose}>
          <Modal.Header>Adset Level Configuration</Modal.Header>
          <Modal.Content>
            <Modal.Description>
              <Form onSubmit={this.handleSubmit}>
                <Grid columns={2}>
                  <Grid.Row>
                    <Grid.Column>
                      <Form.Field>
                        <label>Target Field</label>
                        <Select
                          name="targetField"
                          style={{
                            backgroundColor: "#f7f7f7",
                            borderRadius: "4px",
                            border: "1px solid transparent",
                            borderBottom: "1px dashed #289df3",
                            boxShadow: "0 0 0 0 transparent",
                            color: "#16191c",
                            height: "40px",
                            transition:
                              "box-shadow .15s ease-out .15s,border-color .15s,background .15s ease-out .15s,color .15s ease-out"
                          }}
                          value={targetField}
                          onChange={this.handleSelectChange}
                          options={targetFieldsMapping}
                        />
                      </Form.Field>
                    </Grid.Column>
                    <Grid.Column>
                      <Form.Field>
                        <label>Target CPA</label>
                        <input
                          name="targetCPA"
                          style={this.inputStyle}
                          value={targetCPA}
                          onChange={this.handleChange}
                        />
                      </Form.Field>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
                <Grid columns={1}>
                  <Grid.Row>
                    <Grid.Column>
                      <Form.Field>
                        <label>Activate Multiple Ads</label>
                        <Checkbox
                          name="activeMultipleAds"
                          toggle
                          checked={activeMultipleAds === 1 ? true : false}
                          onChange={() =>
                            this.handleCheckbox(
                              "activeMultipleAds",
                              activeMultipleAds === 1 ? 0 : 1
                            )
                          }
                        />
                      </Form.Field>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
                <Grid columns={1}>
                  <Grid.Row>
                    <Grid.Column>
                      <Form.Field>
                        <label>Ad Level Budget</label>
                        <Checkbox
                          name="adLevelBudget"
                          toggle
                          checked={adLevelBudget === 1 ? true : false}
                          onChange={() =>
                            this.handleCheckbox(
                              "adLevelBudget",
                              adLevelBudget === 1 ? 0 : 1
                            )
                          }
                        />
                      </Form.Field>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
                <Grid columns={1}>
                  <Grid.Row>
                    <Grid.Column>
                      <Form.Field>
                        <label>Manual Ad Rotation</label>
                        <Checkbox
                          name="manualAdRotation"
                          toggle
                          checked={manualAdRotation === 1 ? true : false}
                          onChange={e =>
                            this.handleCheckbox(
                              "manualAdRotation",
                              manualAdRotation === 1 ? 0 : 1
                            )
                          }
                        />
                      </Form.Field>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
                <Grid columns={2}>
                  <Grid.Row>
                    <Grid.Column>
                      <Form.Field>
                        <label>Minimum Budget</label>
                        <input
                          name="minBudget"
                          style={this.inputStyle}
                          value={minBudget}
                          onChange={this.handleChange}
                        />
                      </Form.Field>
                    </Grid.Column>
                    <Grid.Column>
                      <Form.Field>
                        <label>Maximum Budget</label>
                        <input
                          name="maxBudget"
                          style={this.inputStyle}
                          value={maxBudget}
                          onChange={this.handleChange}
                        />
                      </Form.Field>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>

                <Grid columns={2}>
                  <Grid.Row>
                    <Grid.Column>
                      <Form.Field>
                        <label>View Attribution Window</label>
                        <Select
                          name="viewAttr"
                          style={{
                            backgroundColor: "#f7f7f7",
                            borderRadius: "4px",
                            border: "1px solid transparent",
                            borderBottom: "1px dashed #289df3",
                            boxShadow: "0 0 0 0 transparent",
                            color: "#16191c",
                            height: "40px",
                            transition:
                              "box-shadow .15s ease-out .15s,border-color .15s,background .15s ease-out .15s,color .15s ease-out"
                          }}
                          value={viewAttr}
                          onChange={this.handleSelectChange}
                          options={viewAttrFieldMapping}
                        />
                      </Form.Field>
                    </Grid.Column>
                    <Grid.Column>
                      <Form.Field>
                        <label>Click Attribution Window</label>
                        <Select
                          name="swipeAttr"
                          style={{
                            backgroundColor: "#f7f7f7",
                            borderRadius: "4px",
                            border: "1px solid transparent",
                            borderBottom: "1px dashed #289df3",
                            boxShadow: "0 0 0 0 transparent",
                            color: "#16191c",
                            height: "40px",
                            transition:
                              "box-shadow .15s ease-out .15s,border-color .15s,background .15s ease-out .15s,color .15s ease-out"
                          }}
                          value={swipeAttr}
                          onChange={this.handleSelectChange}
                          options={swipeUpFieldMapping}
                        />
                      </Form.Field>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
                <Grid columns={2}>
                  <Grid.Row>
                    <Grid.Column>
                      <Form.Field>
                        <label>CPA Calculation based on (in days)</label>
                        <input
                          name="cpaCalcDays"
                          style={this.inputStyle}
                          value={cpaCalcDays}
                          onChange={this.handleChange}
                        />
                      </Form.Field>
                    </Grid.Column>
                    <Grid.Column>
                      <Form.Field>
                        <label>No of Ads to be active (basic Target CPA)</label>
                        <input
                          name="activeAds"
                          style={this.inputStyle}
                          value={activeAds}
                          onChange={this.handleChange}
                        />
                      </Form.Field>
                    </Grid.Column>
                  </Grid.Row>
                  <Grid.Row
                    style={{ display: "flex", justifyContent: "center" }}
                  >
                    <Form.Field>
                      <Button
                        style={{
                          backgroundColor: "#2185d0",
                          color: "white"
                        }}
                        content="Submit"
                      />
                    </Form.Field>
                  </Grid.Row>
                </Grid>
              </Form>
            </Modal.Description>
          </Modal.Content>
        </Modal>
      </>
    );
  }
}
export default AdsetConfigModal;
