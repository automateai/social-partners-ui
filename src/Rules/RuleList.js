import React, { Component } from "react";
import RuleTable from "./RuleTable";
import {
  Form,
  Select,
  Grid,
  Segment,
  Button,
  Icon,
  Loader
} from "semantic-ui-react";
import GlobalElement from "../config/utility.js";
import ReqRespHandler from "../config/ReqRespHandler.js";
const accountOptions = [{ key: "fb", value: "fb", text: "Facebook" }];

const inputStyle = {
  border: "1px solid #bdbdbd"
};
const labelStyle = {
  color: "#757575"
};

class RuleList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      publisherList: [],
      accountList: [],
      campaignList: [],
      adsetList: [],
      adsList: [],
      selectedPublisher: "",
      selectedAccount: "",
      selectedCampaign: "",
      selectedAdset: "",
      selectedAds: "",
      ruleTable: [],
      showAdsetModal: false,
      showAdsModal: false,
      loading: false,
      ruleTableCount: 0,
      activePageIndex: 1
    };
  }
  componentDidMount = async () => {
    const userData = localStorage.getItem("userData");

    let { loading } = this.state;
    this.setState({ loading: true });
    ReqRespHandler.getCall("publisherList", false, result => {
      if (result[1].success) {
        const publisherResponse = result[1].data;
        const publisherList = publisherResponse.map(function(publisher, key) {
          return {
            key: key,
            value: publisher.publisher_id,
            text: publisher.name
          };
        });
        this.setState({
          publisherList: publisherList,
          loading: false
        });
      } else {
        this.setState({
          publisherList: [],
          loading: false
        });
      }
    });
  };
  updateModalState = () => {
    let { showAdsetModal } = this.state;
    this.setState({ showAdsetModal: !showAdsetModal });
  };
  updateAdsModalState = e => {
    let { showAdsModal } = this.state;
    this.setState({ showAdsModal: !showAdsModal });
  };

  handleSubmit = pageIndex => {
    if (typeof pageIndex !== "number") pageIndex = 1;
    let {
      selectedPublisher,
      selectedAccount,
      selectedCampaign,
      selectedAdset,
      ruleTable,
      loading,
      activePageIndex
    } = this.state;

    this.setState({
      activePageIndex: pageIndex
    });

    let URL = "";
    if (selectedCampaign !== "") {
      URL =
        `/publisher/${selectedPublisher}/account/${selectedAccount}/campaign/${selectedCampaign}/adsets?start=` +
        (pageIndex - 1) * 25;
    } else if (selectedAccount !== "") {
      URL =
        `/publisher/${selectedPublisher}/account/${selectedAccount}/adsets?start=` +
        (pageIndex - 1) * 25;
    }

    this.setState({ loading: true });
    ReqRespHandler.getCall(URL, true, result => {
      if (result[1].success) {
        const ruleTableResponse = result[1].data;
        const ruleTableDataList = ruleTableResponse.map(function(adset, key) {
          return {
            platform: adset.publisher_name,
            adaccount: adset.adaccount_name,
            campaign: adset.campaign_name,
            adset: adset.adset_name,
            adset_rule_config: (
              <Icon
                name="edit"
                style={{ fontSize: 15, cursor: "pointer" }}
                onClick={() => console.log(this)}
              />
            ),
            ad_level_config: (
              <Icon
                name="edit"
                style={{ fontSize: 15, cursor: "pointer" }}
                onClick={e => this.updateAdsModalState.bind(this, e)}
              />
            ),
            icon: (
              <Icon
                name="delete"
                style={{ color: "red", fontSize: 15, fontSize: 15 }}
              />
            )
          };
        });
        this.setState({
          ruleTable: ruleTableResponse,
          ruleTableCount: Math.ceil(result[1].count / 25),
          loading: false
        });
      } else {
        this.setState({
          loading: false,
          ruleTable: []
        });
      }
    });
  };

  fetchAccountsByPublisher = (e, { value }) => {
    let { selectedPublisher, selectedAccount, loading } = this.state;
    this.setState({
      selectedPublisher: value,
      selectedAccount: "",
      selectedCampaign: "",
      selectedAdset: ""
    });
    //fetching Publisher based account
    this.setState({ loading: true });
    ReqRespHandler.getCall(
      `/publisher/${value}/accounts?start=0&limit=500`,
      true,
      result => {
        if (result[1].success) {
          const accountResponse = result[1].data;
          const accountList = accountResponse.map(function(account, key) {
            return {
              key: key,
              value: account.adaccount_id,
              text: account.name
            };
          });
          this.setState({
            accountList: accountList,
            loading: false
          });
        } else {
          this.setState({
            accountList: [],
            loading: false
          });
        }
      }
    );
  };

  fetchDataByAccount = (e, { value }) => {
    //Fetching Publisher and Account based campaign
    let { selectedPublisher, selectedAccount, loading } = this.state;
    this.setState({
      selectedAccount: value,
      selectedCampaign: "",
      selectedAdset: "",
      loading: true
    });
    ReqRespHandler.getCall(
      `/publisher/${selectedPublisher}/account/${value}/campaigns?start=0&limit=500`,
      true,
      result => {
        if (result[1].success) {
          const campaignResponse = result[1].data;
          const campaignList = campaignResponse.map(function(campaign, key) {
            return {
              key: key,
              value: campaign.campaign_id,
              text: campaign.campaign_name
            };
          });
          this.setState({
            campaignList: campaignList
          });
        } else {
          this.setState({
            campaignList: []
          });
        }
      }
    );
    ReqRespHandler.getCall(
      `/publisher/${selectedPublisher}/account/${value}/adsets?start=0&limit=500`,
      true,
      result => {
        if (result[1].success) {
          const adsetResponse = result[1].data;
          const adsetList = adsetResponse.map(function(adset, key) {
            return {
              key: key,
              value: adset.adset_id,
              text: adset.adset_name
            };
          });
          this.setState({
            adsetList: adsetList,
            loading: false
          });
        } else {
          this.setState({
            adsetList: [],
            loading: false
          });
        }
      }
    );
    ReqRespHandler.getCall(
      `/publisher/${selectedPublisher}/account/${value}/ads`,
      true,
      result => {
        if (result[1].success) {
          const adsResponse = result[1].data;
          const adsList = adsResponse.map(function(ads, key) {
            return {
              key: key,
              value: ads.ad_id,
              text: ads.name
            };
          });
          this.setState({
            adsList: adsList
          });
        } else {
          this.setState({
            adsList: []
          });
        }
      }
    );
  };

  fetchDataByCampaign = (e, { value }) => {
    let {
      selectedPublisher,
      selectedAccount,
      selectedCampaign,
      loading
    } = this.state;
    this.setState({
      selectedCampaign: value,
      selectedAdset: "",
      loading: true
    });
    ReqRespHandler.getCall(
      `/publisher/${selectedPublisher}/account/${selectedAccount}/campaign/${value}/adsets`,
      true,
      result => {
        if (result[1].success) {
          const adsetResponse = result[1].data;
          const adsetList = adsetResponse.map(function(adset, key) {
            return {
              key: key,
              value: adset.adset_id,
              text: adset.adset_name
            };
          });
          this.setState({
            adsetList: adsetList,
            loading: false
          });
        } else {
          this.setState({
            adsetList: [],
            loading: false
          });
        }
      }
    );
    ReqRespHandler.getCall(
      `/publisher/${selectedPublisher}/account/${selectedAccount}/campaign/${value}/ads`,
      true,
      result => {
        if (result[1].success) {
          const adsResponse = result[1].data;
          const adsList = adsResponse.map(function(ad, key) {
            return {
              key: key,
              value: ad.ad_id,
              text: ad.name
            };
          });
          this.setState({
            adsList: adsList
          });
        } else {
          this.setState({
            adsList: []
          });
        }
      }
    );
  };

  fetchAdsByAdset = (e, { value }) => {
    let {
      selectedPublisher,
      selectedAccount,
      selectedCampaign,
      selectedAdset,
      activePageIndex
    } = this.state;
    this.setState({
      selectedAdset: value
    });
    ReqRespHandler.getCall(
      `/publisher/${selectedPublisher}/account/${selectedAccount}/campaign/${selectedCampaign}/adset/${value}/ads`,
      true,
      result => {
        if (result[1].success) {
          const adsResponse = result[1].data;
          const adsList = adsResponse.map(function(ad, key) {
            return {
              key: key,
              value: ad.ad_id,
              text: ad.name
            };
          });
          this.setState({
            adsList: adsList
          });
        } else {
          this.setState({
            adsList: []
          });
        }
      }
    );
  };
  render() {
    const {
      accountList,
      publisherList,
      campaignList,
      adsetList,
      adsList,
      ruleTable,
      showAdsetModal,
      showAdsModal,
      selectedPublisher,
      selectedAccount,
      selectedCampaign,
      selectedAdset,
      ruleTableCount,
      activePageIndex,
      loading
    } = this.state;
    console.log(selectedCampaign);
    return (
      <div>
        <Loader active={loading} />
        <Grid columns={1}>
          <Grid.Row>
            <Grid.Column
              style={{ marginLeft: "2%", marginRight: "3%", marginTop: "3%" }}
            >
              <Segment
                style={{
                  borderRadius: 0
                }}
              >
                <Form onSubmit={this.handleSubmit}>
                  <Grid>
                    <Grid.Row columns={2}>
                      <Grid.Column style={{ padding: "0 0 0 15px" }}>
                        <Form.Field>
                          <label style={labelStyle}>Platform</label>
                          <Select
                            placeholder="Select Platform"
                            clearable
                            fluid
                            search
                            selection
                            value={selectedPublisher}
                            options={publisherList}
                            style={inputStyle}
                            onChange={(e, data) =>
                              this.fetchAccountsByPublisher(e, data)
                            }
                          />
                        </Form.Field>
                      </Grid.Column>
                      <Grid.Column style={{ padding: "0 15px 0 15px" }}>
                        <Form.Field>
                          <label style={labelStyle}>Account</label>
                          <Select
                            placeholder="Select Account"
                            clearable
                            fluid
                            search
                            selection
                            value={selectedAccount}
                            options={accountList}
                            style={inputStyle}
                            onChange={(e, data) =>
                              this.fetchDataByAccount(e, data)
                            }
                          />
                        </Form.Field>
                      </Grid.Column>

                      <Grid.Column style={{ padding: "15px 0 0 15px" }}>
                        <Form.Field>
                          <label style={labelStyle}>Campaign</label>
                          <Select
                            placeholder="Select Campaign"
                            clearable
                            fluid
                            search
                            selection
                            value={selectedCampaign}
                            options={campaignList}
                            style={inputStyle}
                            onChange={(e, data) =>
                              this.fetchDataByCampaign(e, data)
                            }
                          />
                        </Form.Field>
                      </Grid.Column>
                      <Grid.Column style={{ padding: "15px 15px 0 15px" }}>
                        <Form.Field>
                          <label style={labelStyle}>Adset</label>
                          <Select
                            placeholder="Select Adset"
                            clearable
                            fluid
                            search
                            selection
                            value={selectedAdset}
                            options={adsetList}
                            style={inputStyle}
                            onChange={(e, data) =>
                              this.fetchAdsByAdset(e, data)
                            }
                          />
                        </Form.Field>
                      </Grid.Column>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Column style={{ textAlign: "center" }}>
                        <Form.Field>
                          <Button
                            type="submit"
                            style={{
                              color: "white",
                              backgroundColor: "#2185d0"
                            }}
                            disabled={selectedAccount === ""}
                          >
                            Submit
                          </Button>
                        </Form.Field>
                      </Grid.Column>
                    </Grid.Row>
                  </Grid>
                </Form>
              </Segment>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column style={{ marginLeft: "2%", marginRight: "3%" }}>
              <RuleTable
                key={Math.random()}
                tableData={ruleTable}
                selectedPublisher={selectedPublisher}
                selectedAccount={selectedAccount}
                selectedCampaign={selectedCampaign}
                refetchTableData={() => this.handleSubmit(activePageIndex)}
                handleSubmit={this.handleSubmit}
                ruleTableCount={ruleTableCount}
                activePage={activePageIndex}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </div>
    );
  }
}

export default RuleList;
