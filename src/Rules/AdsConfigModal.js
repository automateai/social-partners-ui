import React from "react";
import _ from "lodash";
import { isEqual } from "lodash";
import { message } from "antd";
import {
  Modal,
  Form,
  Grid,
  Select,
  Checkbox,
  Segment,
  Table,
  Menu,
  Icon,
  Button,
  Loader
} from "semantic-ui-react";
import styled from "styled-components";
import ReqRespHandler from "../config/ReqRespHandler.js";

const Input = styled.input``;
class AdsConfigModal extends React.Component {
  inputStyle = {
    backgroundColor: "#f7f7f7",
    borderRadius: "4px",
    border: "1px solid transparent",
    borderBottom: "1px dashed #289df3",
    boxShadow: "0 0 0 0 transparent",
    color: "#16191c",
    padding: "0 16px 1px",
    height: "40px",
    transition:
      "box-shadow .15s ease-out .15s,border-color .15s,background .15s ease-out .15s,color .15s ease-out"
  };

  constructor(props) {
    super(props);
    this.state = {
      tableData: this.props.data
    };
  }
  componentDidUpdate = prevProps => {
    let { data } = this.props;
    if (data && data.length > 0 && !isEqual(data, prevProps.data)) {
      this.setState({
        tableData: data
      });
    }
  };

  handleChange(index, dataType, value) {
    const newState = this.state.tableData.map((item, i) => {
      if (i == index) {
        return { ...item, [dataType]: value };
      }
      return item;
    });
    this.setState({
      tableData: newState
    });
  }
  updateAds = index => {
    let { tableData } = this.state;
    let adsList = [];
    adsList.push(tableData[index].ads_id);
    let data = {
      ads_list: adsList,
      ad_rotate_flag: tableData[index].optimization,
      ad_rotate_min_budget: tableData[index].ad_rotate_min_budget,
      ad_rotate_max_budget: tableData[index].ad_rotate_max_budget,
      status: tableData[index].status
    };

    ReqRespHandler.putCall(`/ads`, true, data, result => {
      if (result[1].success) {
        message.success("Successfully Saved!");
      } else {
        message.error("Something went wrong!");
      }
    });
  };
  render() {
    let { data } = this.props;
    let { tableData } = this.state;

    return (
      <>
        <Modal open={this.props.showModal} onClose={this.props.updateAdsStatus}>
          <Modal.Header>Ads Level Configuration</Modal.Header>
          <Modal.Content>
            <Modal.Description>
              <Grid>
                <Grid.Row style={{ padding: 0 }}>
                  <Table sortable singleLine>
                    <Table.Header style={{ textAlign: "left" }}>
                      <Table.Row>
                        <Table.HeaderCell>Ad Name</Table.HeaderCell>
                        <Table.HeaderCell>Status</Table.HeaderCell>
                        <Table.HeaderCell>Optimization</Table.HeaderCell>
                        <Table.HeaderCell>Minimum Budget</Table.HeaderCell>
                        <Table.HeaderCell>Maximum Budget</Table.HeaderCell>
                        <Table.HeaderCell>Action</Table.HeaderCell>
                      </Table.Row>
                    </Table.Header>
                    <Table.Body>
                      {tableData &&
                        tableData.map((data, index) => (
                          <Table.Row key={data.ads_id + Math.random()}>
                            <Table.Cell>{data.ad_name}</Table.Cell>
                            <Table.Cell>
                              <Checkbox
                                toggle
                                checked={data.status === 1 ? true : false}
                                onChange={e =>
                                  this.handleChange(
                                    index,
                                    "status",
                                    data.status === 1 ? 0 : 1
                                  )
                                }
                              />
                            </Table.Cell>
                            <Table.Cell>
                              <Checkbox
                                toggle
                                checked={data.optimization === 1 ? true : false}
                                onChange={e =>
                                  this.handleChange(
                                    index,
                                    "optimization",
                                    data.optimization === 1 ? 0 : 1
                                  )
                                }
                              />
                            </Table.Cell>
                            <Table.Cell>
                              <Input
                                key={index}
                                type="number"
                                value={tableData[index].ad_rotate_min_budget}
                                onChange={e =>
                                  this.handleChange(
                                    index,
                                    "ad_rotate_min_budget",
                                    e.target.value
                                  )
                                }
                              />
                            </Table.Cell>
                            <Table.Cell>
                              <Input
                                key={index}
                                type="number"
                                value={tableData[index].ad_rotate_max_budget}
                                onChange={e =>
                                  this.handleChange(
                                    index,
                                    "ad_rotate_max_budget",
                                    e.target.value
                                  )
                                }
                              />
                            </Table.Cell>
                            <Table.Cell>
                              <Icon
                                name="save"
                                style={{
                                  fontSize: 20,
                                  cursor: "pointer",
                                  color: "green"
                                }}
                                onClick={() => this.updateAds(index)}
                              />
                            </Table.Cell>
                          </Table.Row>
                        ))}
                    </Table.Body>
                  </Table>
                </Grid.Row>
              </Grid>
            </Modal.Description>
          </Modal.Content>
        </Modal>
      </>
    );
  }
}
export default AdsConfigModal;
