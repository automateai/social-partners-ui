import _ from "lodash";
import React, { Component } from "react";
import {
  Table,
  Container,
  Icon,
  Segment,
  Modal,
  Pagination
} from "semantic-ui-react";
import AdsetConfigModal from "./AdsetConfigModal";
import AdsConfigModal from "./AdsConfigModal";
import ReqRespHandler from "../config/ReqRespHandler.js";
import { message } from "antd";

export default class TableExampleSortable extends Component {
  data = [];
  constructor(props) {
    super(props);
    this.state = {
      column: null,
      // data: this.props.tableData,
      direction: null,
      showModal: false,
      showAdsModal: false,
      adsetModalData: [],
      adsModalData: [],
      newGeneratedRuleData: "",
      selectedAdset: "",
      renderTable: false,
      ruleID: null
    };
    this.createTableData = this.createTableData.bind(this);
    this.updateModalState = this.updateModalState.bind(this);
    this.updateAdsetStatus = this.updateAdsetStatus.bind(this);
    this.updateAdsStatus = this.updateAdsStatus.bind(this);
    this.updateAdsModalState = this.updateAdsModalState.bind(this);
  }

  updateAdsetStatus = () => {
    let { showModal } = this.state;
    this.setState({ showModal: !showModal });
  };

  updateAdsStatus = () => {
    let { showAdsModal } = this.state;
    this.setState({ showAdsModal: !showAdsModal });
  };

  updateModalState = (adset_id, rule_id) => {
    let { showModal, selectedAdset, newGeneratedRuleData } = this.state;
    this.setState({ showModal: !showModal, selectedAdset: adset_id });
    if (rule_id) {
      ReqRespHandler.getCall(`/rule/${rule_id}`, true, result => {
        if (result[1].success) {
          const adsetModalDataResponse = result[1].data;
          const adsetModalDataList = adsetModalDataResponse.map(function(
            rule,
            key
          ) {
            return {
              rule_id: rule.rule_id,
              cpa_target_event: rule.cpa_target_event,
              cpa_target_value: rule.cpa_target_value,
              is_multiple_ad: rule.is_multiple_ad,
              is_ad_level_budget: rule.is_ad_level_budget,
              manual_adrotate_ad_not_in_optimization:
                rule.manual_adrotate_ad_not_in_optimization,
              min_budget: rule.min_budget,
              max_budget: rule.max_budget,
              view_attribution_window: rule.view_attribution_window,
              swipe_up_attribution_window: rule.swipe_up_attribution_window,
              cpa_calculation_days: rule.cpa_calculation_days,
              number_of_ads: rule.number_of_ads
            };
          });

          this.setState({
            adsetModalData: adsetModalDataList.filter(
              rule => rule.rule_id === rule_id
            ),
            ruleID: result[1].data[0].rule_id
            // newGeneratedRuleData: adsetModalDataResponse
          });
        } else {
          this.setState({
            adsetModalData: [],
            ruleID: null
          });
        }
      });
    } else {
      this.setState({
        ruleID: null,
        adsetModalData: []
      });
    }
  };

  updateAdsModalState = (
    publisher_id,
    account_id,
    campaign_id,
    adset_id,
    rule_id
  ) => {
    let {
      selectedPublisher,
      selectedAccount,
      selectedCampaign,
      selectedAdset
    } = this.props;
    let { showAdsModal, adsModalData } = this.state;
    this.setState({ showAdsModal: !showAdsModal });
    ReqRespHandler.getCall(
      `/publisher/${publisher_id}/account/${account_id}/campaign/${campaign_id}/adset/${adset_id}/ads`,
      true,
      result => {
        if (result[1].success) {
          const adsModalDataResponse = result[1].data;
          const adsModalDataList = adsModalDataResponse.map(function(ads, key) {
            return {
              ad_name: ads.name,
              ads_id: ads.ad_id,
              status: ads.status,
              optimization: ads.ad_rotate_flag,
              ad_rotate_max_budget: ads.ad_rotate_max_budget,
              ad_rotate_min_budget: ads.ad_rotate_min_budget
            };
          });
          this.setState({
            adsModalData: adsModalDataList
          });
        } else {
          this.setState({
            adsModalData: []
          });
        }
      }
    );
  };

  handleSort = clickedColumn => () => {
    const { column, data, direction } = this.state;

    if (column !== clickedColumn) {
      this.setState({
        column: clickedColumn,
        data: _.sortBy(data, [clickedColumn]),
        direction: "ascending"
      });

      return;
    }

    this.setState({
      data: data.reverse(),
      direction: direction === "ascending" ? "descending" : "ascending"
    });
  };

  createTableData = () => {
    let {
      tableData,
      selectedPublisher,
      selectedAccount,
      selectedCampaign,
      renderTable
    } = this.props;
    this.data = tableData.map(function(adset, key) {
      return {
        platform: adset.publisher_name,
        adaccount: adset.adaccount_name,
        campaign: adset.campaign_name,
        adset: adset.adset_name,
        rule_id: adset.rule_id,
        account_id: adset.adaccount_id,
        campaign_id: adset.campaign_id,
        publisher_id: adset.publisher_id,
        adset_id: adset.adset_id
      };
    });
    tableData = this.data;
  };

  deleteRule = (adset_id, rule_id) => {
    let { cpaCalcDays, activeAds } = this.state;
    let { newGenRuleData, selectedAdset } = this.props;
    let adsetSelected = [];
    adsetSelected.push(adset_id);
    let data = {
      adset_list: adsetSelected,
      rule_id: null
    };
    ReqRespHandler.putCall(`/adset`, true, data, result => {
      if (result[1].success) {
        message.success("Successfully Deleted!");
        this.props.refetchTableData();
      } else {
        message.error("Something went Wrong!");
        this.props.refetchTableData();
      }
    });
  };

  handlePageChange = (e, { activePage }) => {
    this.props.handleSubmit(activePage);
  };
  render() {
    const {
      column,
      data,
      direction,
      showModal,
      showAdsModal,
      adsModalData,
      adsetModalData,
      newGeneratedRuleData,
      selectedAdset,
      ruleID
    } = this.state;
    let { ruleTableCount, refetchTableData, activePage } = this.props;
    this.createTableData();
    return (
      <>
        <AdsetConfigModal
          showModal={showModal}
          updateModalState={this.updateModalState}
          data={adsetModalData ? adsetModalData : []}
          newGenRuleData={newGeneratedRuleData ? newGeneratedRuleData : []}
          selectedAdset={selectedAdset}
          refetchTableData={refetchTableData}
          updateAdsetStatus={this.updateAdsetStatus}
          ruleID={ruleID}
        />
        <AdsConfigModal
          showModal={showAdsModal}
          updateModalState={this.updateAdsModalState}
          data={adsModalData}
          updateAdsStatus={this.updateAdsStatus}
        />
        <Table fixed singleLine striped compact={true}>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell
                // sorted={column === "platform" ? direction : null}
                onClick={this.handleSort("platform")}
                collapsing={false}
              >
                Platform
              </Table.HeaderCell>
              <Table.HeaderCell
                // sorted={column === "adaccount" ? direction : null}
                onClick={this.handleSort("adaccount")}
                collapsing={false}
              >
                Ad Account
              </Table.HeaderCell>
              <Table.HeaderCell
                width={4}
                // sorted={column === "campaign" ? direction : null}
                onClick={this.handleSort("campaign")}
                collapsing={false}
              >
                Campaign
              </Table.HeaderCell>
              <Table.HeaderCell
                width={4}
                // sorted={column === "adset" ? direction : null}
                onClick={this.handleSort("adset")}
                collapsing={false}
              >
                Adset
              </Table.HeaderCell>
              <Table.HeaderCell
                // sorted={column === "adset_rule_config" ? direction : null}
                onClick={this.handleSort("adset_rule_config")}
                collapsing={false}
              >
                Adset Rule Config
              </Table.HeaderCell>
              <Table.HeaderCell
                // sorted={column === "ad_level_config" ? direction : null}
                onClick={this.handleSort("ad_level_config")}
                collapsing={false}
              >
                Ad Level Config
              </Table.HeaderCell>
              <Table.HeaderCell
                // sorted={column === "icon" ? direction : null}
                onClick={this.handleSort("icon")}
                collapsing={false}
              >
                Delete
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {this.data.length > 0 ? (
              _.map(
                this.data,
                ({
                  adaccount,
                  campaign,
                  platform,
                  adset,
                  adset_rule_config,
                  ad_level_config,
                  icon,
                  adset_id,
                  rule_id,
                  account_id,
                  campaign_id,
                  publisher_id
                }) => (
                  <Table.Row key={platform + Math.random()}>
                    <Table.Cell>{platform}</Table.Cell>
                    <Table.Cell
                      style={{ whiteSpace: "normal", wordBreak: "break-all" }}
                      title={adaccount}
                    >
                      {adaccount}
                    </Table.Cell>
                    <Table.Cell
                      style={{ whiteSpace: "normal", wordBreak: "break-all" }}
                      title={campaign}
                      collapsing={true}
                    >
                      {campaign}
                    </Table.Cell>
                    <Table.Cell
                      style={{ whiteSpace: "normal", wordBreak: "break-all" }}
                      title={adset}
                      collapsing={true}
                    >
                      {adset}
                    </Table.Cell>
                    <Table.Cell>
                      <Icon
                        name="edit"
                        style={{ fontSize: 15, cursor: "pointer" }}
                        onClick={() => this.updateModalState(adset_id, rule_id)}
                      />
                    </Table.Cell>
                    <Table.Cell>
                      <Icon
                        name="edit"
                        style={{ fontSize: 15, cursor: "pointer" }}
                        onClick={() =>
                          this.updateAdsModalState(
                            publisher_id,
                            account_id,
                            campaign_id,
                            adset_id,
                            rule_id
                          )
                        }
                      />
                    </Table.Cell>
                    <Table.Cell>
                      {rule_id && (
                        <Icon
                          name="delete"
                          style={{
                            color: "red",
                            fontSize: 15,
                            fontSize: 15,
                            cursor: "pointer"
                          }}
                          onClick={() => this.deleteRule(adset_id, rule_id)}
                        />
                      )}
                    </Table.Cell>
                  </Table.Row>
                )
              )
            ) : (
              <>
                <span
                  style={{
                    height: "250px",
                    display: "inline-block"
                  }}
                ></span>
              </>
            )}
          </Table.Body>
        </Table>
        {this.data.length > 0 ? (
          <div style={{ display: "flex", justifyContent: "center" }}>
            <Pagination
              boundaryRange={0}
              activePage={activePage}
              ellipsisItem={null}
              firstItem={null}
              lastItem={null}
              siblingRange={ruleTableCount}
              totalPages={ruleTableCount}
              onPageChange={this.handlePageChange}
            />
          </div>
        ) : null}
      </>
    );
  }
}
