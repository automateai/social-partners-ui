import React, { Component } from "react";
import {
  Form,
  Dropdown,
  Grid,
  Segment,
  Button,
  Select,
  Checkbox
} from "semantic-ui-react";

class AdsetConfig extends Component {
  countryOptions = [
    { key: "af", value: "af", text: "Afghanistan" },
    { key: "ax", value: "ax", text: "Aland Islands" },
    { key: "al", value: "al", text: "Albania" },
    { key: "dz", value: "dz", text: "Algeria" },
    { key: "as", value: "as", text: "American Samoa" },
    { key: "ad", value: "ad", text: "Andorra" },
    { key: "ao", value: "ao", text: "Angola" },
    { key: "ai", value: "ai", text: "Anguilla" },
    { key: "ag", value: "ag", text: "Antigua" },
    { key: "ar", value: "ar", text: "Argentina" },
    { key: "am", value: "am", text: "Armenia" },
    { key: "aw", value: "aw", text: "Aruba" },
    { key: "au", value: "au", text: "Australia" },
    { key: "at", value: "at", text: "Austria" },
    { key: "az", value: "az", text: "Azerbaijan" },
    { key: "bs", value: "bs", text: "Bahamas" },
    { key: "bh", value: "bh", text: "Bahrain" },
    { key: "bd", value: "bd", text: "Bangladesh" },
    { key: "bb", value: "bb", text: "Barbados" },
    { key: "by", value: "by", text: "Belarus" },
    { key: "be", value: "be", text: "Belgium" },
    { key: "bz", value: "bz", text: "Belize" },
    { key: "bj", value: "bj", text: "Benin" }
  ];
  inputStyle = {
    backgroundColor: "#f7f7f7",
    borderRadius: "4px",
    border: "1px solid transparent",
    boxShadow: "0 0 0 0 transparent",
    color: "#16191c",
    padding: "0 16px 1px",
    height: "40px",
    transition:
      "box-shadow .15s ease-out .15s,border-color .15s,background .15s ease-out .15s,color .15s ease-out"
  };
  render() {
    return (
      <Grid>
        <Grid.Row>
          <Grid.Column style={{ padding: 50 }}>
            <Segment
              className={"w3-animate-bottom"}
              style={{
                borderRadius: 0,
                textAlign: "left"
              }}
            >
              <Form>
                <Grid columns={2}>
                  <Grid.Row>
                    <Grid.Column>
                      <Form.Field>
                        <label>Target Field</label>
                        <Select
                          style={{
                            backgroundColor: "#f7f7f7",
                            borderRadius: "4px",
                            border: "1px solid transparent",
                            boxShadow: "0 0 0 0 transparent",
                            color: "#16191c",
                            height: "40px",
                            transition:
                              "box-shadow .15s ease-out .15s,border-color .15s,background .15s ease-out .15s,color .15s ease-out"
                          }}
                          placeholder="Select target field"
                          options={this.countryOptions}
                        />
                      </Form.Field>
                    </Grid.Column>
                    <Grid.Column>
                      <Form.Field>
                        <label>Target CPA</label>
                        <input style={this.inputStyle} />
                      </Form.Field>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
                <Grid columns={1}>
                  <Grid.Row>
                    <Grid.Column>
                      <Form.Field>
                        <label>Activate Multiple Ads</label>
                        <Checkbox toggle />
                      </Form.Field>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
                <Grid columns={1}>
                  <Grid.Row>
                    <Grid.Column>
                      <Form.Field>
                        <label>Ad Level Budget</label>
                        <Checkbox toggle />
                      </Form.Field>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
                <Grid columns={1}>
                  <Grid.Row>
                    <Grid.Column>
                      <Form.Field>
                        <label>Manual Ad Rotation</label>
                        <Checkbox toggle />
                      </Form.Field>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
                <Grid columns={2}>
                  <Grid.Row>
                    <Grid.Column>
                      <Form.Field>
                        <label>Minimum Budget</label>
                        <input style={this.inputStyle} />
                      </Form.Field>
                    </Grid.Column>
                    <Grid.Column>
                      <Form.Field>
                        <label>Maximum Budget</label>
                        <input style={this.inputStyle} />
                      </Form.Field>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>

                <Grid columns={2}>
                  <Grid.Row>
                    <Grid.Column>
                      <Form.Field>
                        <label>View Attribution Window</label>
                        <input style={this.inputStyle} />
                      </Form.Field>
                    </Grid.Column>
                    <Grid.Column>
                      <Form.Field>
                        <label>Click Attribution Window</label>
                        <input style={this.inputStyle} />
                      </Form.Field>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
                <Grid columns={2}>
                  <Grid.Row>
                    <Grid.Column>
                      <Form.Field>
                        <label>CPA Calculation based on (in days)</label>
                        <input style={this.inputStyle} />
                      </Form.Field>
                    </Grid.Column>
                    <Grid.Column>
                      <Form.Field>
                        <label>No of Ads to be active (basic Target CPA)</label>
                        <input style={this.inputStyle} />
                      </Form.Field>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              </Form>
            </Segment>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}

export default AdsetConfig;
