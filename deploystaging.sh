echo '1. Zipping all files'
find ./build -type f  -exec gzip -9 "{}" \; -exec mv "{}.gz" "{}" \;
echo '2. Deleting everything from bucket'
aws s3 rm s3://automate-360-ui-staging --recursive
echo '3. Pushing code to bucket'
aws s3 sync build/ s3://automate-360-ui-staging --content-encoding gzip --cache-control public,max-age=30672000
echo 'Everything up to date. Hit the URL: http://automate-360-ui-staging.s3-website-us-west-2.amazonaws.com'
